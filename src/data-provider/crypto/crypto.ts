import crypto from "crypto";

export enum HashAlgorithm {
    Md5 = "md5",
    Sha1 = "sha1"
};

export enum Algorithm {
    Sha256 = "sha256",
    Sha512 = "sha512"
};

export enum Digest {
    Base64 = "base64",
    Hex = "hex"
}

export interface Encrypter {
    encrypt(key: string, data: string, digest: Digest): string;
}

abstract class Crypto implements Encrypter {
    protected algorithm: Algorithm;

    protected constructor(algorithm: Algorithm) {
        this.algorithm = algorithm;
    }

    public encrypt(key: string, data: string, digest: Digest): string {
        return crypto.createHmac(this.algorithm, key)
            .update(data)
            .digest(digest);
    }

    public createHash(hashAlgorithm: HashAlgorithm, data: string, digest: Digest) {
        return crypto.createHash(hashAlgorithm)
            .update(data)
            .digest(digest);
    }
}

export class Sha256 extends Crypto {
    constructor() {
        super(Algorithm.Sha256);
    }
}

export class Sha512 extends Crypto {
    constructor() {
        super(Algorithm.Sha512);
    }
}
