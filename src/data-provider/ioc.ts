import {Container, interfaces} from "inversify";
import * as request from 'superagent';
import {DefaultCommandBus} from "./command-bus/command-bus";
import {Core, DataProvider} from "../config/types";
import {BitRobotService} from "./exchange/bitrobot/service";
import {BitRobotHttpClient} from "./exchange/bitrobot/client-http";
import {CommandHandlerMap} from "./command-bus/command-handler-map";
import {CommandHandler} from "../core/command-handler/command-handler";
import {MysqlDataSource} from "./storage/mysql";
import {Config} from "../config/config";
import {DefaultEventBus} from "./event-bus/event-bus";
import {EventBus, EventSubscriber} from "../core/event-bus/event-bus";
import {Sqs} from "./messaging/sqs";
import {StatementRepository} from "./repository/statement-repository";
import {ConfigurationRepository} from "./repository/configuration-repository";
import {CommissionRepository} from "./repository/commission-repository";
import {AccountRepository} from "./repository/account-repository";

export default async function(container: Container, config: Config): Promise<void> {
    await register(container, config);
}

const register = async (container: Container, config: Config) => {
    const dataSource = new MysqlDataSource(config.database);
    dataSource.connect();

    container.bind<MysqlDataSource>(DataProvider.MySqlDataSource).toConstantValue(dataSource);
    container.bind<Sqs>(Core.MessagingProvider).to(Sqs);

    container.bind<AccountRepository>(Core.AccountRepository).to(AccountRepository);
    container.bind<CommissionRepository>(Core.CommissionRepository).to(CommissionRepository);
    container.bind<ConfigurationRepository>(Core.ConfigurationRepository).to(ConfigurationRepository);
    container.bind<StatementRepository>(Core.StatementRepository).to(StatementRepository);

    container.bind<DefaultCommandBus>(DataProvider.CommandBus).to(DefaultCommandBus);
    container.bind<CommandHandlerMap>(DataProvider.CommandHandlerMap).toDynamicValue((context: interfaces.Context): CommandHandlerMap => {
        const commandHandlerMap = new CommandHandlerMap();
        context.container.getAll(Core.CommandHandler.Public).forEach((commandHandler: any) => {
            commandHandlerMap.add(commandHandler.getCommandName(), <CommandHandler>commandHandler);
        });
        context.container.getAll(Core.CommandHandler.Private).forEach((commandHandler: any) => {
            commandHandlerMap.add(commandHandler.getCommandName(), <CommandHandler>commandHandler, true);
        });

        return commandHandlerMap;
    });

    container.bind<EventBus>(Core.EventBus).toDynamicValue((context: interfaces.Context): EventBus => {
        const eventBus = new DefaultEventBus();
        context.container.getAll(Core.EventSubscriber).forEach((eventSubscriber: any) => {
            eventBus.register(<EventSubscriber>eventSubscriber);
        });

        return eventBus;
    });

    container.bind<request.SuperAgent<request.SuperAgentRequest>>(DataProvider.HttpClient).toDynamicValue((): request.SuperAgent<request.SuperAgentRequest> => {
        return request.agent();
    });

    // BitRobot
    container.bind<BitRobotService>(Core.ExchangeService).to(BitRobotService);
    container.bind<BitRobotHttpClient>(DataProvider.Exchange.BitRobot.HttpClient).to(BitRobotHttpClient);

};
