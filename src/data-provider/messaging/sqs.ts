import {injectable} from "inversify";
import {Messaging} from "../../core/messaging/messaging";
import {SQS} from "aws-sdk";
const API_VERSION = '2012-11-05';

@injectable()
export class Sqs implements Messaging {
    private sqs: SQS;

    constructor() {
        this.sqs = new SQS({
            apiVersion: API_VERSION,
        });
    }

    async publish(message: string): Promise<any> {
        const queueUrl: string = `${process.env.SQS_URL_CREATE_ORDER}`;

        return this.sqs.sendMessage({
            QueueUrl: queueUrl,
            MessageBody: message,
            MessageAttributes: {
                // AttributeNameHere: {
                //     StringValue: 'Attribute Value Here',
                //     DataType: 'String',
                // },
            },
        }).promise();
    }
}