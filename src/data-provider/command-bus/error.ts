export class CommandNotFoundError extends Error {
    constructor(commandName: string) {
        super();

        this.message = `Command ${commandName} not found`;
        this.name = Error.name;
        this.stack = "";
    }
}
