import {CommandNotFoundError} from "./error";
import {CommandHandler, CommandHandlerCollection} from "../../core/command-handler/command-handler";

export class CommandHandlerMap {
    private readonly commandHandlerCollection: CommandHandlerCollection;
    private readonly privateCommandHandlerCollection: CommandHandlerCollection;

    constructor(commandHandlerCollection: CommandHandlerCollection = new Map(), privateCommandHandlerCollection: CommandHandlerCollection = new Map()) {
        this.commandHandlerCollection = commandHandlerCollection;
        this.privateCommandHandlerCollection = privateCommandHandlerCollection;
    }

    /**
     *
     * @param name
     * @param commandHandler
     * @param privateCommand
     */
    add(name: string, commandHandler: CommandHandler, privateCommand: boolean = false) {
        if (privateCommand) {
            this.privateCommandHandlerCollection.set(name, commandHandler);
            return;
        }

        this.commandHandlerCollection.set(name, commandHandler);
    }

    /**
     *
     * @param name
     */
    getCommand(name: string): any {
        if (!this.commandHandlerCollection.has(name)) {
            throw new CommandNotFoundError(name);
        }

        return this.commandHandlerCollection.get(name);
    }

    /**
     *
     * @param name
     */
    getPrivateCommand(name: string): any {
        if (!this.privateCommandHandlerCollection.has(name)) {
            throw new CommandNotFoundError(name);
        }

        return this.privateCommandHandlerCollection.get(name);
    }
}
