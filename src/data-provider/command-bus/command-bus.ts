import {CommandHandlerMap} from "./command-handler-map";
import {Command} from "../../core/command/command";
import {CommandBus} from "../../core/command-bus/command-bus";

export class DefaultCommandBus implements CommandBus {
    private _commandHandlerMap: CommandHandlerMap;

    constructor(commandHandlerMap: CommandHandlerMap) {
        this._commandHandlerMap = commandHandlerMap;

    }

    /**
     *
     * @param command
     */
    dispatch(command: Command): Promise<void> {
        const commandHandler = this._commandHandlerMap.getCommand(command.constructor.name);
        return commandHandler.handle(command);
    }
}
