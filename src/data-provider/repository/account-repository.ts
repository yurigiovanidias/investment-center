import {injectable} from 'inversify';
import {BaseRepository} from "./base-repository";
import {AccountRepository as AccountRepositoryInterface} from "../../core/repository/account-repository";
import {Account, Accounts} from "../../core/entity";

@injectable()
export class AccountRepository extends BaseRepository implements AccountRepositoryInterface {
    protected table: string = "accounts";

    async findAllActive(): Promise<Accounts> {
        try {
            const result = await this.queryBuilder(this.table)
                .where("active", true);

            if (result.length <= 0) {
                return [];
            }

            return result.map((row: any) => {
                return Object.assign(new Account(), row);
            })
        }
        catch (e) {
            console.log(e);
            throw e;
        }
    }

    async findActiveByStrategy(id: number): Promise<Accounts> {
        try {
            const result = await this.queryBuilder(this.table)
                .where("active", true)
                .where("strategy_id", id);

            if (result.length <= 0) {
                return [];
            }

            return result.map((row: any) => {
                return Object.assign(new Account(), row);
            })
        }
        catch (e) {
            console.log(e);
            throw e;
        }
    }

    async findByApiKey(apiKey: string): Promise<Account> {
        try {
            const result = await this.queryBuilder(this.table).where("api_key", apiKey).limit(1).first();

            if (typeof result === "undefined") {
                return new Account();
            }

            return Object.assign(new Account(), result);
        }
        catch (e) {
            console.log(e);
            throw e;
        }
    }
}
