import {inject, injectable} from 'inversify';
import Client from "knex";
import {DataProvider} from "../../config/types";
import {MysqlDataSource} from "../storage/mysql";

@injectable()
export class BaseRepository {
    protected table: string = "";
    protected readonly dataSource: MysqlDataSource;
    protected readonly queryBuilder: Client;

    constructor(
        @inject(DataProvider.MySqlDataSource) mysqlDataSource: MysqlDataSource) {
        this.dataSource = mysqlDataSource;
        this.queryBuilder = this.dataSource.getQueryBuilder();
    }
}
