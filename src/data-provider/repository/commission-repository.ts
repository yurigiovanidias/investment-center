import {inject, injectable} from 'inversify';
import {Core, DataProvider} from "../../config/types";
import {BaseRepository} from "./base-repository";
import {CommissionRepository as CommissionRepositoryInterface} from "../../core/repository/commission-repository";
import {MysqlDataSource} from "../storage/mysql";
import {Commission, Commissions} from "../../core/value-object";
import {CommissionFactory} from "../../core/factory/commission-factory";

@injectable()
export class CommissionRepository extends BaseRepository implements CommissionRepositoryInterface {
    private commissionFactory: CommissionFactory;
    protected table: string = "commissions";

    constructor(
        @inject(DataProvider.MySqlDataSource) mysqlDataSource: MysqlDataSource,
        @inject(Core.CommissionFactory) commissionFactory: CommissionFactory
    ) {
        super(mysqlDataSource);

        this.commissionFactory = commissionFactory;
    }

    async findByAccountId(accountId: number, orderBy: string = "ASC", limit: number = 0): Promise<Commissions> {
        try {
            const query = this.queryBuilder(this.table)
                .where("account_id", accountId)
                .orderBy("created", orderBy)

            const result = await query;

            if (result.length <= 0) {
                return [];
            }

            return result.map((row: any) => {
                return this.commissionFactory.create(
                    row.id, row.hash, row.account_id, row.currency_id,
                    row.amount, row.description, row.created
                );
            });
        }
        catch (e) {
            console.log(e);
            throw e;
        }
    }

    async save(commission: Commission): Promise<Array<number>> {
        const result = await this.queryBuilder(this.table).insert(commission);
        commission.setId(result[0]);

        return result;
    }
}
