import {inject, injectable} from 'inversify';
import {Core, DataProvider} from "../../config/types";
import {BaseRepository} from "./base-repository";
import {ConfigurationRepository as ConfigurationRepositoryInterface} from "../../core/repository/configuration-repository";
import {MysqlDataSource} from "../storage/mysql";
import {Configuration, ConfigurationCollection} from "../../core/entity";
import {ConfigurationFactory} from "../../core/factory/configuration-factory";

@injectable()
export class ConfigurationRepository extends BaseRepository implements ConfigurationRepositoryInterface {
    protected table: string = "configurations";
    private configurationFactory: ConfigurationFactory;

    constructor(
        @inject(DataProvider.MySqlDataSource) mysqlDataSource: MysqlDataSource,
        @inject(Core.ConfigurationFactory) configurationFactory: ConfigurationFactory,
    ) {
        super(mysqlDataSource);

        this.configurationFactory = configurationFactory;
    }

    async findByAccountId(accountId: number): Promise<ConfigurationCollection> {
        try {
            const query = this.queryBuilder(this.table).where("account_id", accountId);
            const result = await query;

            if (result.length <= 0) {
                return new Map<string, string>();
            }

            const configurations = result.map((row: any) => {
                return (new Configuration())
                    .setName(row.name)
                    .setValue(row.value)
                    .setCreated(row.created)
                    .setUpdated(row.updated)
            });
            return this.configurationFactory.createCollection(configurations);
        }
        catch (e) {
            console.log(e);
            throw e;
        }
    }
}
