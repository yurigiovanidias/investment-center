import {inject, injectable} from 'inversify';
import {Core, DataProvider} from "../../config/types";
import {BaseRepository} from "./base-repository";
import {StatementRepository as StatementRepositoryInterface} from "../../core/repository/statement-repository";
import {MysqlDataSource} from "../storage/mysql";
import {Statement, Statements} from "../../core/value-object";
import {StatementFactory} from "../../core/factory/statement-factory";

@injectable()
export class StatementRepository extends BaseRepository implements StatementRepositoryInterface {
    private statementFactory: StatementFactory;
    protected table: string = "statements";

    constructor(
        @inject(DataProvider.MySqlDataSource) mysqlDataSource: MysqlDataSource,
        @inject(Core.StatementFactory) statementFactory: StatementFactory
    ) {
        super(mysqlDataSource);

        this.statementFactory = statementFactory;
    }

    /**
     *
     * @param id
     */
    async findById(id: number): Promise<Statement> {
        try {
            const result = await this.queryBuilder(this.table).where("id", id).limit(1).first();

            if (typeof result === "undefined") {
                return new Statement();
            }

            const statement: Statement = Object.assign(new Statement(), result);
            return statement;
        }
        catch (e) {
            console.log(e);
            throw e;
        }

    }

    async findByAccountId(accountId: number, orderBy: string = "ASC", limit: number = 0): Promise<Statements> {
        try {
            const query = this.queryBuilder(this.table)
                .where("account_id", accountId)
                .orderBy("created", orderBy)

            const result = await query;

            if (result.length <= 0) {
                return [];
            }

            return result.map((row: any) => {
                return this.statementFactory.create(
                    row.id, row.account_id, row.input_currency_id,
                    row.input, row.output_currency_id, row.output, row.type, row.created
                );
            });
        }
        catch (e) {
            console.log(e);
            throw e;
        }
    }

    async save(statement: Statement): Promise<Array<number>> {
        return this.queryBuilder(this.table).insert(statement);
    }
}
