import {injectable} from "inversify";
import Client from "knex";
import {StorageConfig} from "../../config/storage/config";

@injectable()
// @ts-ignore
export class MysqlDataSource {
    public queryBuilder?: Client;
    private readonly database: string = "";
    private readonly host: string = "";
    private port: number = 0;
    private readonly password: string = "";
    private readonly user: string = "";
    private readonly multipleStatements: boolean = false;

    constructor(config: StorageConfig) {
        this.database = config.database;
        this.host = config.host;
        this.port = config.port;
        this.password = config.password;
        this.user = config.user;
        this.multipleStatements = config.multipleStatements;
    }

    async connect() {
        if (this.queryBuilder) {
            return;
        }

        try {
            this.queryBuilder = Client({
                client: 'mysql2',
                connection: {
                    host : this.host,
                    user : this.user,
                    password : this.password,
                    database : this.database
                },
                pool: { min: 1, max: 1 }
            });

            // this.queryBuilder = await mysql.createPool({
            //     host: this.host,
            //     user: this.user,
            //     password: this.password,
            //     database: this.database,
            //     connectionLimit: 1,
            //     multipleStatements: this.multipleStatements,
            // });
        } catch (e) {
            throw new Error(e.message);
        }
    }

    public getQueryBuilder(): Client {
        return this.queryBuilder!;
    }
}
