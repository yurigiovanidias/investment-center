export class NotFoundExchangeServiceError extends Error {
    constructor(exchangeId: number) {
        super();

        this.message = `ExchangeService ${exchangeId} not found`;
        this.name = NotFoundExchangeServiceError.name;
        this.stack = "";
    }
}
