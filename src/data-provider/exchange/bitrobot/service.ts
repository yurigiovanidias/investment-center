import {inject, injectable} from "inversify";
import {Core, DataProvider} from "../../../config/types";
import {BitRobotHttpClient} from "./client-http";
import {
    CreateOperationRequest,
    GetBalanceRequest, GetOrdersRequest,
    GetIndexRequest,
    GetOperationsRequest,
    GetSnapshotsRequest,
    GetSubaccountRequest, GetCandlesRequest
} from "./request";
import {CreateOperationResponse, GetBalanceResponse, GetOrdersResponse} from "./response";
import {ExchangeAccount} from "../../../core/entity";
import {
    Balance, Candle,
    IndexSymbol,
    Operation,
    Operations,
    Order,
    Orders, Side,
    Snapshot,
    Snapshots, Status,
    Symbol, TypeOrder
} from "../../../core/value-object";
import {ExchangeService} from "../../../core/exchange/service";
import {OperationFactory} from "../../../core/factory/operation-factory";

@injectable()
export class BitRobotService implements ExchangeService {
    private client: BitRobotHttpClient;
    private operationFactory: OperationFactory;

    constructor(
        @inject(Core.OperationFactory) operationFactory: OperationFactory,
        @inject(DataProvider.Exchange.BitRobot.HttpClient) client: BitRobotHttpClient
    ) {
        this.client = client;
        this.operationFactory = operationFactory;
    }

    /**
     *
     * @param exchange
     * @param symbol
     */
    async getIndex(exchange: string, symbol: Symbol): Promise<Array<IndexSymbol>> {
        const indexRequest = new GetIndexRequest(symbol.getId());

        const result = await this.client.index(indexRequest);

        return result.map((index) => {
            return new IndexSymbol(index.exchange, index.symbol, index.price);
        });
    }

    /**
     *
     * @param exchange
     * @param symbol
     * @param from
     * @param to
     */
    async getCandles(exchange: string, symbol: Symbol, from: string, to: string): Promise<Array<IndexSymbol>> {
        const candlesRequest = new GetCandlesRequest(exchange, symbol.getId(), '1d', from, to);
        const result = await this.client.candles(candlesRequest);

        // @ts-ignore
        return result.map((candle) => {
            return new Candle(exchange, candle.symbol, candle.open_time, candle.open, candle.high, candle.low, candle.close, candle.close_time);
        });
    }

    /**
     *
     * @param apiKey
     * @param operation
     */
    async createOperation(apiKey: string, operation: Operation): Promise<Operation> {
        const operationRequest = new CreateOperationRequest(
            this.getCredentials(apiKey),
            operation.getCurrencyId(),
            operation.getType(),
            operation.getSubtype(),
            operation.getDecimalAmount(),
            operation.getDescription(),
        );
        const createOperationResponse: CreateOperationResponse = <any>await this.client.createOperation(operationRequest);

        operation.setId(createOperationResponse.id);

        return operation;
    }

    /**
     *
     * @param apiKey
     */
    async getBalance(apiKey: string): Promise<Array<Balance>> {
        const balanceRequest = new GetBalanceRequest(this.getCredentials(apiKey));
        const balanceResponse: GetBalanceResponse = <any>await this.client.getBalance(balanceRequest);

        return balanceResponse.map(balance => {
            return new Balance(balance.currency, balance.available_amount, balance.locked_amount);
        });
    }

    async getOperations(apiKey: string, from: string, to: string): Promise<Operations> {
        const operationsRequest = new GetOperationsRequest(this.getCredentials(apiKey), from, to);
        const operationsResponse: Array<any> = <any>await this.client.getOperations(operationsRequest);

        return operationsResponse.map(operation => {
            return this.operationFactory.create(
                operation.id,
                operation.account_id,
                operation.subaccount_id,
                operation.order_id,
                operation.currency_id,
                operation.amount,
                operation.type,
                operation.subtype,
                operation.description,
                operation.created
            );
        });
    }

    async getOrders(apiKey: string, status: Array<Status>, from: string, to: string): Promise<Orders> {
        const getOrdersRequest = new GetOrdersRequest(this.getCredentials(apiKey), status, from, to);
        const orderResponse: GetOrdersResponse = await this.client.getOrders(getOrdersRequest);

        return orderResponse.map(order => {
            return (new Order())
                .setId(order.id)
                .setAccountId(order.account_id)
                .setSubaccountId(order.subaccount_id)
                .setSymbolId(order.symbol_id)
                .setExternalId(order.external_id)
                .setIntegerAmount(order.amount)
                .setIntegerPrice(order.price)
                .setSide(<Side>order.side)
                .setType(<TypeOrder>order.type)
                .setStatus(<Status>order.status)
                .setCreated(order.created)
                .setUpdated(order.updated);
        });
    }

    async getSnapshots(apiKey: string, from: string, to: string): Promise<Snapshots> {
        const snapshotsRequest = new GetSnapshotsRequest(this.getCredentials(apiKey), from, to);
        const snapshotsResponse: Array<any> = <any>await this.client.getSnapshots(snapshotsRequest);

        return snapshotsResponse.map(row => {
            return (new Snapshot())
                .setAccountId(row.account_id)
                .setSubaccountId(row.subaccount_id)
                .setCurrencyId(row.currency_id)
                .setIntegerAmount(row.amount)
                .setCreated(row.created)
        });
    }

    async getAccountAndSubaccount(apiKey: string): Promise<ExchangeAccount> {
        const getSubaccountRequest = new GetSubaccountRequest({
            secret_key: "",
            api_key: apiKey,
        });

        console.log(getSubaccountRequest);

        const result =  await this.client.getSubaccount(getSubaccountRequest);
        const account = new ExchangeAccount();
        account.setId(result.account_id )
            .setUser(result.user_id)
            .setExchange(result.exchange_id)
            .setTakerFee(result.taker_fee)
            .setMakerFee(result.maker_fee);

        // const subaccount = new Subaccount()
        //
        // subaccount.setId(result.id)
        //     .setExchangeId(result.exchange_id)
        //     .setAccountId(result.account_id)
        //     .setApiKey(result.api_key)
        //     .setName(result.name)
        //     .setCreated(result.created);
        //
        // account.setSubaccount(subaccount);

        return account;
    }

    public getExchangeId(): number {
        return 1;
    }

    /**
     *
     * @private
     * @param apiKey
     */
    private getCredentials(apiKey: string) {
        return {
            api_key: apiKey,
            secret_key: "",
        }
    }
}
