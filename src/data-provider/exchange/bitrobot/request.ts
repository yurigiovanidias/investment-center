import {BitRobotConfig} from "./config";
import {Credential, Headers, Method, Request} from "../../../core/exchange/http-client/exchange-http-request";
import {Status, Subtype, Type} from "../../../core/value-object";

export const CreateOperationCommand: string = "create_operation";
export const GetBalanceCommand: string = "get_balance";
export const GetIndexUri: string = "index";
export const GetCandlesUri: string = "candles";
export const GetOperationsCommand: string = "get_operations";
export const GetExecutedOrdersCommand: string = "get_orders";
export const GetSubaccountCommand: string = "get_subaccount";
export const GetSnapshotsCommand: string = "get_snapshots";

abstract class BitRobotRequest implements Request {
    protected method: Method = Method.Post;
    protected credentials?: Credential;

    setCredentials(value: Credential): void {
        this.credentials = value;
    }

    getMethod(): Method {
        return this.method!;
    }

    public abstract getUrl(): string;

    public abstract body(): any;

    public abstract getHeaders(params?: any): Headers;
}

abstract class PrivateRequest extends BitRobotRequest {
    protected abstract command_name: string;

    constructor(credentials: Credential) {
        super();

        this.setCredentials(credentials);
    }

    getHeaders(params?: any): Headers {
        const config = new BitRobotConfig;

        return {
            'Content-Type': 'application/json',
            'x-api-key': this.credentials!.api_key,
        };
    }

    getUrl(): string {
        const config = new BitRobotConfig;
        return `${config.privateUrl}/`;
    }

    getBody(): any {
        return {
            command_name: this.command_name,
        };
    }
}

abstract class PublicRequest extends BitRobotRequest {
    getHeaders(params?: any): Headers {
        return {
            'Content-Type': 'application/json',
        };
    }

    getUrl(): string {
        const config = new BitRobotConfig;
        return `${config.publicUrl}/`;
    }
}

export class GetIndexRequest extends PublicRequest {
    public symbol: string = "";

    constructor(symbol: string) {
        super();

        this.symbol = symbol;
        this.method = Method.Get;
    }

    getUrl(): string {
        const config = new BitRobotConfig;
        return `${config.publicUrl}/${GetIndexUri}/${this.symbol}`;
    }

    body(): any {
        return;
    }
}

export class GetBalanceRequest extends PrivateRequest {
    protected command_name: string = GetBalanceCommand;

    body(): any {
        return {
            ...super.getBody(),
        };
    }
}

export class GetOperationsRequest extends PrivateRequest {
    protected command_name: string = GetOperationsCommand;
    private readonly from_created: string;
    private readonly to_created: string;

    constructor(credentials: Credential, from: string, to: string) {
        super(credentials);

        this.from_created = from;
        this.to_created = to;
    }

    body(): any {
        return {
            ...super.getBody(),
            from_created: this.from_created,
            to_created: this.to_created,
        };
    }
}

export class GetOrdersRequest extends PrivateRequest {
    protected command_name: string = GetExecutedOrdersCommand;
    private readonly from_created: string;
    private readonly to_created: string;
    private readonly status: Array<Status>;

    constructor(credentials: Credential, status: Array<Status>, from: string, to: string) {
        super(credentials);

        this.from_created = from;
        this.to_created = to;
        this.status = status;
    }

    body(): any {
        return {
            ...super.getBody(),
            from_created: this.from_created,
            to_created: this.to_created,
            status: this.status,
        };
    }
}

export class GetSnapshotsRequest extends PrivateRequest {
    protected command_name: string = GetSnapshotsCommand;
    private readonly from_date: string;
    private readonly to_date: string;

    constructor(credentials: Credential, from: string, to: string) {
        super(credentials);

        this.from_date = from;
        this.to_date = to;
    }

    body(): any {
        return {
            ...super.getBody(),
            from_date: this.from_date,
            to_date: this.to_date,
        };
    }
}

export class CreateOperationRequest extends PrivateRequest {
    public readonly command_name: string = CreateOperationCommand;
    public readonly currency: string;
    public readonly type: string;
    public readonly subtype: string;
    public readonly amount: number;
    public readonly description: string;

    constructor(credentials: Credential, currency: string, type: Type, subtype: Subtype, amount: number, description: string) {
        super(credentials);

        this.currency = currency;
        this.type = type;
        this.subtype = subtype;
        this.amount = amount;
        this.description = description;
    }

    body(): any {
        return {
            ...super.getBody(),
            currency: this.currency,
            type: this.type,
            subtype: this.subtype,
            amount: this.amount,
            description: this.description,
        };
    }
}

export class GetSubaccountRequest extends PrivateRequest {
    protected command_name: string = GetSubaccountCommand;

    body(): any {
        return {
            ...super.getBody(),
        };
    }
}

export class GetCandlesRequest extends PublicRequest {
    public exchange: string = "";
    public symbol: string = "";
    public interval: string = "";
    public start_date: string = "";
    public end_date: string = "";

    constructor(exchange: string, symbol: string, interval: string, startDate: string, endDate: string) {
        super();

        this.exchange = exchange;
        this.symbol = symbol;
        this.interval = interval;
        this.start_date = startDate;
        this.end_date = endDate;
        this.interval = interval;
        this.method = Method.Get;
    }

    getUrl(): string {
        const config = new BitRobotConfig;
        return `${config.publicUrl}/${GetCandlesUri}/${this.symbol}`;
    }

    body(): any {
        return {
            exchange: this.exchange,
            interval: this.interval,
            start_date: this.start_date,
            end_date: this.end_date,
        };
    }
}
