export type CandleResponse = Array<Candle>;

export interface Candle {
    exchange: string;
    symbol: string;
    open_time: number;
    open: number;
    high: number;
    low: number;
    close: number;
    close_time: number;
}

export type IndexResponse = Array<Index>;

export interface Index {
    exchange: string;
    symbol: string;
    price: number;
}

export type GetBalanceResponse = Array<CurrencyBalance>

export interface CurrencyBalance {
    available_amount: number;
    locked_amount: number;
    currency: string;
}

export interface DefaultResponse {
    code: number;
    data: any;
}

export interface CreateOperationResponse {
    id: number;
    account_id: number;
    subaccount_id: number;
    currency_id: string;
    amount: number;
    type: string;
    subtype: string;
    created: Date;
}

export interface GetSubaccountResponse {
    id: number;
    account_id: number;
    user_id: number;
    exchange_id: number;
    name: string;
    description?: string;
    maker_fee: number;
    taker_fee: number;
    active: boolean;
    created: Date;
}

export interface GetOperationsResponse {
    id: number;
    account_id: number;
    subaccount_id: number;
    currency_id: string;
    amount: number;
    type: string;
    subtype: string;
    description: string;
    created: Date;
    order_id?: number;
}

export type GetOrdersResponse = Array<GetOrderResponse>;

export interface GetOrderResponse {
    id: number;
    account_id: number;
    subaccount_id: number;
    symbol_id: string;
    external_id: string;
    amount: number;
    price: number;
    side: string;
    type: string;
    // fee: number;
    // fills: any[];
    status: string;
    created: Date;
    metadata: string;
    updated: Date;
}
