import {ExchangeConfig} from "../../../core/exchange/config/exchange-config";

export class BitRobotConfig implements ExchangeConfig {
    readonly name: string = "BitRobot";
    readonly privateUrl: string = "https://mmxbj5lvpf.execute-api.us-east-1.amazonaws.com/production/tapi";
    readonly publicUrl: string = "https://2g3k85lhgi.execute-api.us-east-1.amazonaws.com/production/";
}
