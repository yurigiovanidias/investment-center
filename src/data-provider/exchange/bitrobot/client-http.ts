import {injectable} from "inversify";
import {AbstractExchangeHttpClient} from "../../../core/exchange/http-client/exchange-http-client";
import {Request} from "../../../core/exchange/http-client/exchange-http-request";
import {
    CreateOperationRequest,
    GetBalanceRequest,
    GetOrdersRequest,
    GetIndexRequest,
    GetOperationsRequest,
    GetSnapshotsRequest,
    GetSubaccountRequest
} from "./request";
import {
    DefaultResponse,
    GetBalanceResponse,
    IndexResponse,
    GetSubaccountResponse,
    GetOperationsResponse,
    GetOrdersResponse, CandleResponse
} from "./response";
import {createByErrorCode} from "../../../core/error/error";

@injectable()
/**
 * @description Remover todos os métodos e deixar apenas um, o que modificará será apenas o request
 */
export class BitRobotHttpClient extends AbstractExchangeHttpClient {
    async candles(request: GetIndexRequest): Promise<CandleResponse> {
        return this.doPublicQuery(request);
    }

    async index(request: GetIndexRequest): Promise<IndexResponse> {
        return this.doPublicQuery(request);
    }

    async getBalance(request: GetBalanceRequest): Promise<GetBalanceResponse> {
        return this.doPrivateQuery(request);
    }

    async getOperations(request: GetOperationsRequest): Promise<GetOperationsResponse> {
        return this.doPrivateQuery(request);
    }

    async getOrders(request: GetOrdersRequest): Promise<GetOrdersResponse> {
        return this.doPrivateQuery(request);
    }

    async getSnapshots(request: GetSnapshotsRequest): Promise<GetBalanceResponse> {
        return this.doPrivateQuery(request);
    }

    async createOperation(request: CreateOperationRequest): Promise<GetBalanceResponse> {
        return this.doPrivateQuery(request);
    }

    /**
     *
     * @param request
     */
    async getSubaccount(request: GetSubaccountRequest): Promise<GetSubaccountResponse> {
        return this.doPrivateQuery(request);
    }

    private async doPublicQuery(params: Request): Promise<any> {
        try {
            return this.query(params);
        }
        catch (e) {
            throw e;
        }
    }

    /**
     *
     * @param params
     */
    private async doPrivateQuery(params: Request): Promise<any> {
        try {
            const result = await this.query(params);
            return this.getResponse(result);
        }
        catch (e) {

            throw e;
        }
    }

    private getResponse(response: DefaultResponse): any {
        if (response.code === 10000) {
            return response.data!;
        }

        throw createByErrorCode(response.code, response.data);
    }
}
