export const Exchanges: any = {
    b2u: {
        id: 7, acronym: "B2U",
    },
    binance: {
        id: 9, acronym: "BN",
    },
    bitcambio: {
        id: 5, acronym: "BCM",
    },
    bitcointrade: {
        id: 1, acronym: "BTCT",
    },
    bitpreco: {
        id: 2, acronym: "BPR",
    },
    bitrecife: {
        id: 6, acronym: "BRCF",
    },
    emx: {
        id: 4, acronym: "EMX",
    },
    mercadoBitcoin: {
        id: 3, acronym: "MB",
    },
    novadax: {
        id: 8, acronym: "NVD",
    },
};
