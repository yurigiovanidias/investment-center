import {injectable} from "inversify"
import {EventEmitter} from "events";
import {EventBus as EventEmitterInterface, EventSubscriber} from "../../core/event-bus/event-bus";

@injectable()
export class DefaultEventBus implements EventEmitterInterface {
    private innerEmitter: EventEmitter;
    
    constructor() {
        this.innerEmitter = new EventEmitter;
    }

    /**
     *
     * @param eventSubscriber
     */
    register(eventSubscriber: EventSubscriber) {
        const listeners = eventSubscriber.getSubscribedEvents();

        for (let [name, listener] of listeners) {
            listener.map((row) => {
                this.listen(name, row);
            });
        }
    }

    /**
     *
     * @param eventName
     * @param event
     */
    async dispatch(eventName: string, event: any) {
        return this.innerEmitter.emit(eventName, event);
    }

    /**
     *
     * @param name
     * @param listener
     */
    listen(name: string, listener: any) {
        this.innerEmitter.on(name, listener);
    }
}
