import {APIGatewayProxyEvent, APIGatewayProxyResult, Context} from "aws-lambda";
import {Core, DataProvider} from "../../config/types";
import {Application} from "../../application/application";
import {Command} from "../../core/command/command";
import {CommandHandlerMap} from "../../data-provider/command-bus/command-handler-map";
import {CommandHandler} from "../../core/command-handler/command-handler";
import {AccountService} from "../../core/service/account-service";
import {InvalidApiKeyError} from "../../core/error/error";

export const handle = async (
    event: APIGatewayProxyEvent,
    context: Context,
): Promise<APIGatewayProxyResult> => {
    try {
        const response = await tapiHandle(event, context);

        return {
            statusCode: 200,
            body: JSON.stringify(response),
        }
    }
    catch (e) {
        return {
            statusCode: 400,
            body: JSON.stringify(e.message),
        }
    }
};

const tapiHandle = async (event: APIGatewayProxyEvent, context: Context): Promise<TapiResponse> => {
    const app = await startApplication();

    const body = <Command>JSON.parse(<string>event.body);
    const header = event.headers;
    const handlerMap = <CommandHandlerMap>app.get(DataProvider.CommandHandlerMap);

    try {
        console.log(`Handle ${body.command_name} command`, JSON.stringify(body));

        const commandHandler = <CommandHandler>handlerMap.getPrivateCommand(body.command_name);

        body.aws_request_id = context.awsRequestId;

        await commandHandler.handle(body);

        return {
            code: 10000,
            data: body.data,
        }
    }
    catch (e) {
        console.log(`Some error while executing PAPI: ${e.message}`);

        let err = {
            message: e.message,
            internal: 99999,
            code: 500,
        };

        if (Object.getPrototypeOf(e.constructor).name === "AbstractError") {
            err = {
                message: e.message,
                internal: e.internalCode,
                code: e.statusCode,
            }
        }

        return {
            code: err.internal,
            data: e.message,
        }
    }
}

const startApplication = async () => {
    const app = new Application();
    await app.run();

    return app;
}

interface TapiResponse {
    readonly code: number;
    readonly data: any;
}