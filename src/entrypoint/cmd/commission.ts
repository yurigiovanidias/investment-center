import {
    ScheduledEvent
} from "aws-lambda";
import {AccountService} from "../../core/service/account-service";
import {Core, DataProvider} from "../../config/types";
import {CommandHandlerMap} from "../../data-provider/command-bus/command-handler-map";
import {CalculateCommissionCommandHandler} from "../../core/command-handler";
import {Application} from "../../application/application";
import {CalculateCommission, CalculateCommissionCommand} from "../../core/command";
import {AccountRepository} from "../../data-provider/repository/account-repository";

export const calculate = async (
    event: ScheduledEvent
): Promise<void> => {
    const app = await startApplication();
    const accountService = <AccountService>app.get(Core.AccountService);
    const accountRepository = <AccountRepository>app.get(Core.AccountRepository);
    const handlerMap = <CommandHandlerMap>app.get(DataProvider.CommandHandlerMap);
    const accounts = await accountRepository.findAllActive();
    const commandHandler = <CalculateCommissionCommandHandler>handlerMap.getCommand(CalculateCommissionCommand);

    console.log(`Handle ${CalculateCommissionCommand} command`);

    for (let account of accounts) {
        const accountInfo = await accountService.getAccountInfo(account.getApiKey());
        const command = <CalculateCommission>{
            command_name: CalculateCommissionCommand,
            account_info: accountInfo,
        }

        try {
            await commandHandler.handle(command);
        }
        catch (e) {
            console.log(`Some error while executing command ${CalculateCommissionCommand}`, JSON.stringify(command), e.message);
        }
    }
};

const startApplication = async () => {
    const app = new Application();
    await app.run();

    return app;
}
