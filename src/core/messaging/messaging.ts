export interface Messaging {
    publish(message: string): Promise<void>;
}
