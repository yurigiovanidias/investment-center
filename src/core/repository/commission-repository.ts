import {Commission, Commissions} from "../value-object";

export interface CommissionRepository {
    findByAccountId(accountId: number): Promise<Commissions>;
    save(commission: Commission): Promise<Array<number>>;
}
