import {Statement, Statements} from "../value-object";

export interface StatementRepository {
    findById(id: number): Promise<Statement>;
    findByAccountId(accountId: number): Promise<Statements>;
    save(operation: Statement): Promise<Array<number>>;
}
