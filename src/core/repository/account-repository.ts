import {Account, Accounts} from "../entity";

export interface AccountRepository {
    findAllActive(): Promise<Accounts>;
    findActiveByStrategy(id: number): Promise<Accounts>;
    findByApiKey(apiKey: string): Promise<Account>;
}
