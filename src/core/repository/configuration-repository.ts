import {ConfigurationCollection} from "../entity";

export interface ConfigurationRepository {
    findByAccountId(id: number): Promise<ConfigurationCollection>;
}
