import {Command} from "../command/command";

export interface CommandBus {
    dispatch(command: Command): void
}
