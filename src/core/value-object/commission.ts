export type Commissions = Array<Commission>;

export class Commission {
    // @ts-ignore
    private id: number = 0;
    private hash: string = "";
    private account_id: number = 0;
    private currency_id: string = "";
    private amount: number = 0;
    private description?: string = "";
    private created: Date = new Date();

    getId(): number {
        return this.id;
    }

    setId(value: number): Commission {
        this.id = value;
        return this;
    }

    getHash(): string {
        return this.hash;
    }

    setHash(value: string): Commission {
        this.hash = value;
        return this;
    }

    getAccountId(): number {
        return this.account_id;
    }

    setAccountId(value: number): Commission {
        this.account_id = value;
        return this;
    }

    getCurrencyId(): string {
        return this.currency_id;
    }

    setCurrencyId(value: string): Commission {
        this.currency_id = value;
        return this;
    }

    getDecimalAmount(): number {
        return this.amount / 1e8;
    }

    getIntegerAmount(): number {
        return this.amount;
    }

    setDecimalAmount(value: number): Commission {
        this.amount = parseInt((value * 1e8).toFixed(0));
        return this;
    }

    setIntegerAmount(value: number): Commission {
        this.amount = value;
        return this;
    }

    getDescription(): string | undefined {
        return this.description;
    }

    setDescription(value: string | undefined): Commission {
        this.description = value;
        return this;
    }

    getCreated(): Date {
        return this.created;
    }

    setCreated(value: Date): Commission {
        this.created = value;
        return this;
    }
}
