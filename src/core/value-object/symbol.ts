export class Symbol {
    // @ts-ignore
    private id: string = "";
    private base: string = "";
    private quote: string = "";
    private created: Date = new Date();

    constructor(id: string) {
        const [base, quote] = id.split("_");

        this.setId(id);
        this.setBase(base);
        this.setQuote(quote);
    }

    getId(): string {
        return this.id;
    }

    setId(value: string): Symbol {
        this.id = value;
        return this;
    }

    getBase(): string {
        return this.base;
    }

    setBase(value: string): Symbol {
        this.base = value;
        return this;
    }

    getQuote(): string {
        return this.quote;
    }

    setQuote(value: string): Symbol {
        this.quote = value;
        return this;
    }

    getCreated(): Date {
        return this.created;
    }

    setCreated(value: Date) {
        this.created = value;
    }
}
