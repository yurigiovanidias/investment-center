export interface BalanceList {
    [currency: string]: Balance;
}

export class Balance {
    // @ts-ignore
    private readonly currency: string;
    private readonly available_amount: number = 0;
    private readonly locked_amount: number = 0;

    constructor(currency: string, availableAmount: number, lockedAmount: number) {
        this.currency = currency;
        this.available_amount = availableAmount;
        this.locked_amount = lockedAmount;
    }

    getSymbol(): string {
        return this.currency;
    }

    getAvailable(): number {
        return this.available_amount;
    }

    getDecimalAvailable(): number {
        return this.available_amount / 1e8;
    }

    getLocked(): number {
        return this.locked_amount;
    }

    getDecimalLocked(): number {
        return this.locked_amount / 1e8;
    }

    getIntegerTotal(): number {
        return this.getAvailable() + this.getLocked();
    }

    getDecimalTotal(): number {
        return this.getDecimalAvailable() + this.getDecimalLocked();
    }
}
