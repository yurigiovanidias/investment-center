export enum Type {
    Credit = "credit",
    Debit = "debit"
}

export enum Subtype {
    Fee = "fee",
    Quote = "quote",
    Base = "base",
}

export type Operations = Array<Operation>;

export class Operation {
    // @ts-ignore
    private id: number = 0;
    private account_id: number = 0;
    private subaccount_id: number = 0;
    private order_id?: number;
    private currency_id: string = "";
    private amount: number = 0;
    private type: Type = Type.Credit;
    private subtype: Subtype = Subtype.Base;
    private description: string = "";
    private metadata?: string;
    private created: Date = new Date();

    getId(): number {
        return this.id;
    }

    setId(value: number): Operation {
        this.id = value;
        return this;
    }

    getAccountId(): number {
        return this.account_id;
    }

    setAccountId(value: number): Operation {
        this.account_id = value;
        return this;
    }

    getSubaccountId(): number {
        return this.subaccount_id;
    }

    setSubaccountId(value: number): Operation {
        this.subaccount_id = value;
        return this;
    }

    getOrderId(): number | undefined {
        return this.order_id;
    }

    setOrderId(value: number): Operation {
        this.order_id = value;
        return this;
    }

    getDecimalAmount(): number {
        return this.amount / 1e8;
    }

    getIntegerAmount(): number {
        return this.amount;
    }

    setDecimalAmount(value: number): Operation {
        this.amount = parseInt((value * 1e8).toFixed(0));
        return this;
    }

    setIntegerAmount(value: number): Operation {
        this.amount = value;
        return this;
    }

    getType(): Type {
        return this.type;
    }

    setType(value: Type): Operation {
        this.type = value;
        return this;
    }

    getSubtype(): Subtype {
        return this.subtype;
    }

    setSubtype(value: Subtype): Operation {
        this.subtype = value;
        return this;
    }

    getCurrencyId(): string {
        return this.currency_id;
    }

    setCurrencyId(value: string): Operation {
        this.currency_id = value;
        return this;
    }

    getDescription(): string {
        return this.description;
    }

    setDescription(value: string): Operation {
        this.description = value;
        return this;
    }

    getMetadata(): any {
        if (typeof this.metadata === "undefined") {
            return {};
        }

        return JSON.parse(this.metadata);
    }

    getStringMetadata(): string | undefined {
        return this.metadata;
    }

    setMetadata(value: string): Operation {
        this.metadata = value;
        return this;
    }

    getCreated(): Date {
        return this.created;
    }

    setCreated(value: Date): Operation {
        this.created = value;
        return this;
    }

    public isCredit(): boolean {
        return this.type === Type.Credit;
    }
}
