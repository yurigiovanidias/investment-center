export class IndexSymbol {
    // @ts-ignore
    private readonly exchange: string = "";
    private readonly symbol: string = "";
    private readonly price: number = 0;

    constructor(exchange: string, symbol: string, price: number) {
        this.exchange = exchange;
        this.price = price;
        this.symbol = symbol;
    }

    public getExchange() {
        return this.exchange;
    }

    public getPrice() {
        return this.price;
    }

    public getSymbol() {
        return this.symbol;
    }
}
