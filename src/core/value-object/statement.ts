export enum Type {
    Deposit = "deposit",
    Withdraw = "withdraw"
}

export type Statements = Array<Statement>;

export class Statement {
    // @ts-ignore
    private id: number = 0;
    private account_id: number = 0;
    private input_currency_id: string = "";
    private input: number = 0;
    private output_currency_id: string = "";
    private output: number = 0;
    private type: Type = Type.Deposit;
    private description: string = "";
    private created: Date = new Date();

    getId(): number {
        return this.id;
    }

    setId(value: number): Statement {
        this.id = value;
        return this;
    }

    getAccountId(): number {
        return this.account_id;
    }

    setAccountId(value: number): Statement {
        this.account_id = value;
        return this;
    }

    getInputCurrencyId(): string {
        return this.input_currency_id;
    }

    setInputCurrencyId(value: string): Statement {
        this.input_currency_id = value;
        return this;
    }

    getDecimalInput(): number {
        return this.input / 1e8;
    }

    getIntegerInput(): number {
        return this.input;
    }

    setDecimalInput(value: number): Statement {
        this.input = parseInt((value * 1e8).toFixed(0));
        return this;
    }

    setIntegerInput(value: number): Statement {
        this.input = value;
        return this;
    }

    getOutputCurrencyId(): string {
        return this.output_currency_id;
    }

    setOutputCurrencyId(value: string): Statement {
        this.output_currency_id = value;
        return this;
    }

    getDecimalOutput(): number {
        return this.output / 1e8;
    }

    getIntegerOutput(): number {
        return this.output;
    }

    setDecimalOutput(value: number): Statement {
        this.output = parseInt((value * 1e8).toFixed(0));
        return this;
    }

    setIntegerOutput(value: number): Statement {
        this.output = value;
        return this;
    }

    getType(): Type {
        return this.type;
    }

    setType(value: Type): Statement {
        this.type = value;
        return this;
    }

    getDescription(): string {
        return this.description;
    }

    setDescription(value: string): Statement {
        this.description = value;
        return this;
    }

    getCreated(): Date {
        return this.created;
    }

    setCreated(value: Date): Statement {
        this.created = value;
        return this;
    }

    public isDeposit(): boolean {
        return this.type === Type.Deposit;
    }
}
