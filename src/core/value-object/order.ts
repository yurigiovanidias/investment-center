export enum Type {
    Credit = "credit",
    Debit = "debit"
}

export enum Status {
    Executed = "executed"
}

export enum Side {
    Buy = "buy",
    Sell = "sell"
}

export type Orders = Array<Order>;

export class Order {
    // @ts-ignore
    private id: number = 0;
    private account_id: number = 0;
    private subaccount_id: number = 0;
    private symbol_id: string = "";
    private external_id: string = "";
    private amount: number = 0;
    private price: number = 0;
    private side: Side = Side.Buy;
    private type: Type = Type.Credit;
    private status: Status = Status.Executed;
    private created: Date = new Date();
    private updated: Date = new Date();

    getId(): number {
        return this.id;
    }

    setId(value: number): Order {
        this.id = value;
        return this;
    }

    getAccountId(): number {
        return this.account_id;
    }

    setAccountId(value: number): Order {
        this.account_id = value;
        return this;
    }

    getSubaccountId(): number {
        return this.subaccount_id;
    }

    setSubaccountId(value: number): Order {
        this.subaccount_id = value;
        return this;
    }

    getSymbolId(): string {
        return this.symbol_id;
    }

    setSymbolId(value: string): Order {
        this.symbol_id = value;
        return this;
    }

    getExternalId(): string {
        return this.external_id;
    }

    setExternalId(value: string): Order {
        this.external_id = value;
        return this;
    }

    getDecimalAmount(): number {
        return this.amount / 1e8;
    }

    getIntegerAmount(): number {
        return this.amount;
    }

    setDecimalAmount(value: number): Order {
        this.amount = parseInt((value * 1e8).toFixed(0));
        return this;
    }

    setIntegerAmount(value: number): Order {
        this.amount = value;
        return this;
    }

    getDecimalPrice(): number {
        return this.price / 1e8;
    }

    getIntegerPrice(): number {
        return this.price;
    }

    setDecimalPrice(value: number): Order {
        this.price = parseInt((value * 1e8).toFixed(0));
        return this;
    }

    setIntegerPrice(value: number): Order {
        this.price = value;
        return this;
    }

    getSide(): Side {
        return this.side;
    }

    setSide(value: Side): Order {
        this.side = value;
        return this;
    }

    getType(): Type {
        return this.type;
    }

    setType(value: Type): Order {
        this.type = value;
        return this;
    }

    getStatus(): Status {
        return this.status;
    }

    setStatus(value: Status): Order {
        this.status = value;
        return this;
    }

    getCreated(): Date {
        return this.created;
    }

    setCreated(value: Date): Order {
        this.created = value;
        return this;
    }

    getUpdated(): Date {
        return this.updated;
    }

    setUpdated(value: Date): Order {
        this.updated = value;
        return this;
    }
}
