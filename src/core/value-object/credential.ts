export class Credential {
    // @ts-ignore
    private api_key: string = "";
    private secret_key: string = "";

    getApiKey(): string {
        return this.api_key;
    }

    setApiKey(value: string): Credential {
        this.api_key = value;
        return this;
    }

    getSecretKey(): string {
        return this.secret_key;
    }

    setSecretKey(value: string): Credential {
        this.secret_key = value;
        return this;
    }
}
