import {Account, ExchangeAccount, Subaccount} from "../entity";
import {User} from "../entity/user";

export class AccountInfo {
    private user?: User
    private account?: Account;
    private exchange_account?: ExchangeAccount;
    private subaccount?: Subaccount;

    setUser(value: User): AccountInfo {
        this.user = value;
        return this;
    }

    getUser(): User {
        return this.user!;
    }

    getAccount(): Account {
        return this.account!;
    }

    setAccount(value: Account): AccountInfo {
        this.account = value;
        return this;
    }

    getExchangeAccount(): ExchangeAccount {
        return this.exchange_account!;
    }

    setExchangeAccount(value: ExchangeAccount): AccountInfo {
        this.exchange_account = value;
        return this;
    }

    getSubaccount(): Subaccount {
        return this.subaccount!;
    }

    setSubaccount(value: Subaccount): AccountInfo {
        this.subaccount = value;
        return this;
    }
}
