export class Candle {
    // @ts-ignore
    private readonly exchange: string = "";
    private readonly symbol: string = "";
    private readonly open_time: number = 0;
    private readonly close_time: number = 0;
    private readonly open: number = 0;
    private readonly high: number = 0;
    private readonly low: number = 0;
    private readonly close: number = 0;

    constructor(exchange: string, symbol: string, openTime: number, open: number, high: number, low: number, close: number, closeTime: number) {
        this.exchange = exchange;
        this.open_time = openTime;
        this.close_time = closeTime;
        this.open = open;
        this.high = high;
        this.low = low;
        this.close = close;
        this.symbol = symbol;
    }

    public getExchange() {
        return this.exchange;
    }

    public getSymbol() {
        return this.symbol;
    }
}
