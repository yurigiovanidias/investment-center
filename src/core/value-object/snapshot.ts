export enum Type {
    Credit = "credit",
    Debit = "debit"
}

export enum Subtype {
    Fee = "fee",
    Quote = "quote",
    Base = "base",
}

export type Snapshots = Array<Snapshot>;

export class Snapshot {
    // @ts-ignore
    private id: number = 0;
    private account_id: number = 0;
    private subaccount_id: number = 0;
    private currency_id: string = "";
    private amount: number = 0;
    private created: Date = new Date();

    getId(): number {
        return this.id;
    }

    setId(value: number): Snapshot {
        this.id = value;
        return this;
    }

    getAccountId(): number {
        return this.account_id;
    }

    setAccountId(value: number): Snapshot {
        this.account_id = value;
        return this;
    }

    getSubaccountId(): number {
        return this.subaccount_id;
    }

    setSubaccountId(value: number): Snapshot {
        this.subaccount_id = value;
        return this;
    }

    getCurrencyId(): string {
        return this.currency_id;
    }

    setCurrencyId(value: string): Snapshot {
        this.currency_id = value;
        return this;
    }

    getDecimalAmount(): number {
        return this.amount / 1e8;
    }

    getIntegerAmount(): number {
        return this.amount;
    }

    setDecimalAmount(value: number): Snapshot {
        this.amount = parseInt((value * 1e8).toFixed(0));
        return this;
    }

    setIntegerAmount(value: number): Snapshot {
        this.amount = value;
        return this;
    }

    getCreated(): Date {
        return this.created;
    }

    setCreated(value: Date): Snapshot {
        this.created = value;
        return this;
    }
}
