import {injectable, inject} from "inversify";
import {EventSubscriber} from "../event-bus/event-bus";
import {CreatedStatement, CreatedStatementEvent} from "../event/created-statement";

@injectable()
export class StatementListener implements EventSubscriber {
    getSubscribedEvents(): Map<string, Array<EventListener>> {
        const listenersMapped = new Map();

        listenersMapped.set(CreatedStatementEvent, [
            this.createdStatement.bind(this)
        ]);

        return listenersMapped;
    }

    /**
     *
     * @param event
     * @private
     */
    private async createdStatement(event: CreatedStatement): Promise<void> {
        console.log('Created Statement: ', event.getData());
    }
}
