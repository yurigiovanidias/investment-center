import {Balance, IndexSymbol, Operation, Operations, Orders, Status, Symbol} from "../value-object";
import {ExchangeAccount} from "../entity";

export interface ExchangeService {
    getAccountAndSubaccount(apiKey: string): Promise<ExchangeAccount>;
    createOperation(apiKey: string, operation: Operation): Promise<Operation>;
    getBalance(apiKey: string): Promise<Array<Balance>>;
    getOperations(apiKey: string, from: string, to: string): Promise<Operations>;
    getOrders(apiKey: string, status: Array<Status>, from: string, to: string): Promise<Orders>;
    getSnapshots(apiKey: string, from: string, to: string): Promise<any>;
    getCandles(exchange: string, symbol: Symbol, from: string, to: string): Promise<Array<IndexSymbol>>;
    getIndex(exchange: string, symbol: Symbol): Promise<Array<IndexSymbol>>;
    getExchangeId(): number;
}
