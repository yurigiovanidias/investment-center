export interface ExchangeConfig {
    readonly name: string;
    readonly privateUrl: string;
    readonly publicUrl: string;
}
