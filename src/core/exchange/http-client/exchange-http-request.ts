import {Digest} from "../../../data-provider/crypto/crypto";

export interface Credential {
    api_key: string,
    secret_key: string,
}

export type Headers = object;

export enum Method {
    Delete= "DELETE",
    Get= "GET",
    Patch= "PATCH",
    Put= "PUT",
    Post= "POST",
}

export interface Request {
    // getMethod(): Method;
    getUrl(): string;
    getHeaders(params?: any): Headers;
    setCredentials(value: Credential): void;
    body(): object;
}

abstract class AbstractRequest implements Request {
    protected credentials!: Credential;

    protected constructor(credentials: Credential) {
        this.setCredentials(credentials);
    }

    protected abstract encrypt(key: string, data: any, digest: Digest): string;

    public abstract setCredentials(value: Credential): void;

    public abstract getHeaders(): Headers;

    public abstract getUrl(): string;

    public body(): object {
        return JSON.parse(JSON.stringify(this));
    }
}
