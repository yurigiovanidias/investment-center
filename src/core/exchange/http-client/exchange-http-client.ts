import {inject, injectable} from "inversify";
import {SuperAgentStatic} from "superagent";
import {ExchangeConfig} from "../config/exchange-config";
import {Method, Request} from "./exchange-http-request";
import {DataProvider} from "../../../config/types";

export interface ExchangeHttpClient {
    getBalance(base: string, quote: string): Promise<string>
}

@injectable()
export abstract class AbstractExchangeHttpClient {
    private client: SuperAgentStatic;
    protected attemptsRequest = 0;

    /**
     *
     * @param client
     */
    constructor(
        @inject(DataProvider.HttpClient) client: SuperAgentStatic) {
        this.client = client;
    }

    /**
     *
     * @param request
     */
    protected async query(request: Request) {
        const newReq = <any>request;
        let req = null;

        if (newReq.getMethod() === Method.Post) {
            req = await this.client.post(request.getUrl())
                .set('User-Agent', 'node-superagent/5.0.2')
                .set('Content-Type', 'application/json')
                .set(request.getHeaders(request.body()))
                .send(request.body());
        }
        else if (newReq.getMethod() === Method.Delete) {
            req = await this.client.delete(request.getUrl())
                .set('User-Agent', 'node-superagent/5.0.2')
                .set('Content-Type', 'application/json')
                .set(request.getHeaders(request.body()))
                .send(request.body());
        } else {
            req = await this.client.get(request.getUrl())
                .set('User-Agent', 'node-superagent/5.0.2')
                .set('Content-Type', 'application/json')
                .set(request.getHeaders(request.body()))
                .query(request.body());
        }

        this.attemptsRequest++;

        const res = await req;
        if (res.type === 'text/plain' || res.type === 'text/html') {
            return JSON.parse(res.text);
        }

        return res.body;
    }

    /**
     *
     * @param ms
     */
    protected sleep(ms: number) {
        return new Promise((resolve) => {
            setTimeout(resolve, ms);
        });
    }
}
