export abstract class AbstractError extends Error {
    public readonly internalCode: number = 99999;
    public readonly statusCode: number = 500;
    public readonly message: string = "Internal Error"

    protected constructor(message: string, internalCode: number, statusCode: number) {
        super(message);

        this.message = message;
        this.internalCode = internalCode;
        this.statusCode = statusCode;
    }
}

export const errorCodes = {
    commandNotFound: {
        internal: 10002,
        statusCode: 404,
    },
    invalidApiKey: {
        internal: 10003,
        statusCode: 400,
    },
    badGateway: {
        internal: 10004,
        statusCode: 502,
    },
    socketHangUp: {
        internal: 10005,
        statusCode: 400,
    },
    internalServerError: {
        internal: 10006,
        statusCode: 500,
    },
    gatewayTimeout: {
        internal: 10007,
        statusCode: 504,
    },
    orderNotFound: {
        internal: 20001,
        statusCode: 404,
    },
    orderAmountTooSmall: {
        internal: 20002,
        statusCode: 400,
    },
    orderAmountInvalid: {
        internal: 20003,
        statusCode: 400,
    },
    insufficientBalance: {
        internal: 20007,
        statusCode: 400,
    },
    orderWasCanceled: {
        internal: 20008,
        statusCode: 400,
    },
    orderWasExecuted: {
        internal: 20009,
        statusCode: 400,
    },
    orderStillOpen: {
        internal: 200010,
        statusCode: 400,
    },
    transferIntoSubaccounts: {
        internal: 20011,
        statusCode: 400,
    },
    orderPriceTooSmall: {
        internal: 20012,
        statusCode: 400,
    },
    orderPriceInvalid: {
        internal: 20013,
        statusCode: 400,
    },
    tooManyRequests: {
        internal: 20014,
        statusCode: 429,
    },
    orderWasRejected: {
        internal: 20015,
        statusCode: 400,
    },
    invalidSymbol: {
        internal: 20016,
        statusCode: 400,
    },
    invalidQuote: {
        internal: 20017,
        statusCode: 400,
    },
}

export class InvalidApiKeyError extends AbstractError {
    constructor() {
        super("Invalid Api Key", errorCodes.invalidApiKey.internal, errorCodes.invalidApiKey.statusCode);
    }
}

export class BadGatewayError extends AbstractError {
    constructor() {
        super("Bad Gateway", errorCodes.badGateway.internal, errorCodes.badGateway.statusCode);
    }
}

export class SocketHangUpError extends AbstractError {
    constructor() {
        super("Socket hang up", errorCodes.socketHangUp.internal, errorCodes.socketHangUp.statusCode);
    }
}

export class InternalServerError extends AbstractError {
    constructor() {
        super("Internal error", errorCodes.internalServerError.internal, errorCodes.internalServerError.statusCode);
    }
}

export class GatewayTimeoutError extends AbstractError {
    constructor() {
        super("Gateway Timeout error", errorCodes.gatewayTimeout.internal, errorCodes.gatewayTimeout.statusCode);
    }
}

export class CommandNotFoundError extends AbstractError {
    constructor() {
        super("Command not found", errorCodes.commandNotFound.internal, errorCodes.commandNotFound.statusCode);
    }
}

export class InsufficientBalanceError extends AbstractError {
    constructor(message?: string) {
        if (!message) {
            message = "Insufficient balance";
        }

        super(message, errorCodes.insufficientBalance.internal, errorCodes.insufficientBalance.statusCode);
    }
}

export class OrderAmountTooSmallError extends AbstractError {
    constructor() {
        super("Order amount is too small", errorCodes.orderAmountTooSmall.internal, errorCodes.orderAmountTooSmall.statusCode);
    }
}

export class OrderAmountInvalidError extends AbstractError {
    constructor() {
        super("Order amount is invalid", errorCodes.orderAmountInvalid.internal, errorCodes.orderAmountInvalid.statusCode);
    }
}

export class OrderStillOpenError extends AbstractError {
    constructor() {
        super("Order still open", errorCodes.orderStillOpen.internal, errorCodes.orderStillOpen.statusCode);
    }
}

export class OrderNotFoundError extends AbstractError {
    constructor() {
        super("Order not found", errorCodes.orderNotFound.internal, errorCodes.orderNotFound.statusCode);
    }
}

export class OrderWasCanceledError extends AbstractError {
    constructor() {
        super("Order was canceled", errorCodes.orderWasCanceled.internal, errorCodes.orderWasCanceled.statusCode);
    }
}

export class OrderWasExecutedError extends AbstractError {
    constructor() {
        super("Order was executed", errorCodes.orderWasExecuted.internal, errorCodes.orderWasExecuted.statusCode);
    }
}

export class OrderWasRejectedError extends AbstractError {
    constructor() {
        super("Order was rejected", errorCodes.orderWasRejected.internal, errorCodes.orderWasRejected.statusCode);
    }
}

export class OrderPriceTooSmallError extends AbstractError {
    constructor() {
        super("Order price is too small", errorCodes.orderPriceTooSmall.internal, errorCodes.orderAmountTooSmall.statusCode);
    }
}

export class OrderPriceInvalidError extends AbstractError {
    constructor() {
        super("Order price is invalid", errorCodes.orderPriceInvalid.internal, errorCodes.orderPriceInvalid.statusCode);
    }
}

export class InvalidSymbolError extends AbstractError {
    constructor() {
        super(`Invalid symbol` , errorCodes.invalidSymbol.internal, errorCodes.invalidSymbol.statusCode);
    }
}

export class InvalidQuoteError extends AbstractError {
    constructor() {
        super(`Invalid quote` , errorCodes.invalidQuote.internal, errorCodes.invalidQuote.statusCode);
    }
}

export class TransferIntoSubaccountsError extends AbstractError {
    constructor(message?: string) {
        if (!message) {
            message = "Error to transfer into subaccounts";
        }

        super(message, errorCodes.transferIntoSubaccounts.internal, errorCodes.transferIntoSubaccounts.statusCode);
    }
}

export class TooManyRequestsError extends AbstractError {
    constructor() {
        super("Too many requests", errorCodes.tooManyRequests.internal, errorCodes.tooManyRequests.statusCode);
    }
}

export const createByErrorCode = (code: number, message?: string) => {
    switch (code) {
        case errorCodes.orderAmountInvalid.internal:
            return new OrderAmountInvalidError();
        case errorCodes.orderAmountTooSmall.internal:
            return new OrderAmountTooSmallError();
        case errorCodes.orderNotFound.internal:
            return new OrderNotFoundError();
        case errorCodes.invalidApiKey.internal:
            return new InvalidApiKeyError();
        case errorCodes.insufficientBalance.internal:
            return new InsufficientBalanceError();
        case errorCodes.orderStillOpen.internal:
            return new OrderStillOpenError();
        case errorCodes.orderWasCanceled.internal:
            return new OrderWasCanceledError();
        case errorCodes.orderWasExecuted.internal:
            return new OrderWasExecutedError();
        case errorCodes.orderWasRejected.internal:
            return new OrderWasRejectedError();
        default:
            return new Error(`Unknown Error: ${message}`);
    }
}
