import {ExchangeEvent} from "./exchange-event";
import {Statement} from "../value-object";

export const CreatedStatementEvent = "CreatedStatement";

export class CreatedStatement implements ExchangeEvent {
    private readonly statement: Statement;

    constructor(statement: Statement) {
        this.statement = statement;
    }

    getStatement(): Statement {
        return this.statement;
    }

    getData(): any {
        return JSON.stringify(this.statement);
    }

    getEventName(): string {
        return CreatedStatementEvent;
    }
}
