export interface EventBus {
    /**
     * Method to dispatch an event
     * @param name
     * @param event
     */
    dispatch(name: string, event: any): void;

    /**
     * Method to listen an event dispatched
     * @param name
     * @param listener
     */
    listen(name: string, listener: any): void;
}

export type EventSubscriberCollection = Map<string, EventSubscriber>;

export interface EventSubscriber {
    getSubscribedEvents(): Map<string, Array<EventListener>>;
}
