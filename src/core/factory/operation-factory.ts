import {injectable} from "inversify"
import {Operation, Subtype, Type} from "../value-object";

@injectable()
export class OperationFactory {
    /**
     *
     * @param id
     * @param accountId
     * @param subaccountId
     * @param orderId
     * @param currencyId
     * @param amount
     * @param type
     * @param subtype
     * @param description
     * @param created
     */
    public create(
        id: number, accountId: number, subaccountId: number, orderId: number, currencyId: string,
        amount: number, type: string, subtype: string, description: string, created: Date
    ): Operation {
        const operation = this.createWithoutId(accountId, orderId, currencyId, amount, type, subtype, description, created);
        operation.setId(id);

        return operation;
    }

    public createWithoutId(
        accountId: number, orderId: number | undefined, currencyId: string,
        amount: number, type: string, subtype: string, description: string, created: Date
    ): Operation {
        const operation = new Operation();

        if (type === Type.Debit) {
            amount = Math.abs(amount) * -1;
        }

        operation.setAccountId(accountId)
            .setCurrencyId(currencyId)
            .setIntegerAmount(amount)
            .setType(<Type>type)
            .setSubtype(<Subtype>subtype)
            .setDescription(description)
            .setCreated(created);

        if (typeof orderId !== "undefined") {
            operation.setOrderId(orderId);
        }

        return operation;
    }
}

