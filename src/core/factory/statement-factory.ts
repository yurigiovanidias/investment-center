import {injectable} from "inversify"
import {Statement, StatementType} from "../value-object";

@injectable()
export class StatementFactory {
    public create(
        id: number, accountId: number, inputCurrencyId: string,
        input: number, outputCurrencyId: string, output: number, type: StatementType, created: Date
    ): Statement {
        const statement = this.createWithoutId(accountId, inputCurrencyId, input, outputCurrencyId, output, type, created);
        statement.setId(id);

        return statement;
    }

    public createWithoutId(
        accountId: number, inputCurrencyId: string,
        input: number, outputCurrencyId: string, output: number, type: StatementType,
        created: Date
    ): Statement {
        const statement = new Statement();

        if (type === StatementType.Withdraw) {
            input = Math.abs(input) * -1;
            output = Math.abs(output) * -1;
        }

        statement.setAccountId(accountId)
            .setInputCurrencyId(inputCurrencyId)
            .setIntegerInput(input)
            .setOutputCurrencyId(outputCurrencyId)
            .setIntegerOutput(output)
            .setType(type)

        return statement;
    }
}

