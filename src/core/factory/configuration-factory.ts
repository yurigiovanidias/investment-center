import {injectable} from "inversify"
import {Configuration, ConfigurationCollection, Configurations} from "../entity/configuration";

@injectable()
export class ConfigurationFactory {
    public create(
        id: number, accountId: number, name: string, value: string, created: Date
    ): Configuration {
        const configuration = this.createWithoutId(accountId, name, value, created);
        // configuration.setId(id);

        return configuration;
    }

    public createWithoutId(
        accountId: number, name: string, value: string, created: Date = new Date()
    ): Configuration {
        const configuration = new Configuration();

        configuration.setAccountId(accountId)
            .setName(name)
            .setValue(value)
            .setCreated(created);

        return configuration;
    }

    public createCollection(configurations: Configurations): ConfigurationCollection {
        const collection = new Map<string, string>();

        configurations.forEach(configuration => {
            collection.set(configuration.getName(), configuration.getValue());
        });

        return collection;
    }
}
