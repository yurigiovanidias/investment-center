import {injectable} from "inversify"
import {Commission} from "../value-object";

@injectable()
export class CommissionFactory {
    public create(
        id: number, hash: string, accountId: number, currencyId: string,
        amount: number, description: string, created: Date
    ): Commission {
        const commission = this.createWithoutId(accountId, currencyId, amount, description, created);
        commission.setId(id);
        commission.setHash(hash);

        return commission;
    }

    public createWithoutId(
        accountId: number, currencyId: string, amount: number, description?: string, created: Date = new Date()
    ): Commission {
        return (new Commission())
            .setHash(`${(new Date).getTime()}`)
            .setAccountId(accountId)
            .setCurrencyId(currencyId)
            .setIntegerAmount(amount)
            .setDescription(description)
            .setCreated(created);
    }
}

