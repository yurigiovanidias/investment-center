import {injectable} from "inversify"
import {Subaccount} from "../entity";

export interface CreateSubaccount {
    account_id: number,
    exchange_id: number,
    name: string,
    description: string,
    api_key: string,
    secret_key: string,
}

@injectable()
export class SubaccountFactory {
    /**
     *
     * @param id
     * @param accountId
     * @param exchangeId
     * @param apiKey
     * @param secretKey
     * @param name
     * @param description
     * @param created
     */
    public create(
        accountId: number, exchangeId: number, apiKey: string, secretKey: string,
        name: string, description: string | undefined, created: Date
    ): Subaccount{
        const subaccount = new Subaccount();

        subaccount.setAccountId(accountId)
            .setExchangeId(exchangeId)
            .setApiKey(apiKey)
            .setSecretKey(secretKey)
            .setName(name)
            .setDescription(description)
            .setCreated(created);

        return subaccount;
    }

    public createWithId(
        id: number, accountId: number, exchangeId: number, apiKey: string, secretKey: string,
        name: string, description: string | undefined, created: Date
    ): Subaccount{
        const subaccount = this.create(accountId, exchangeId, apiKey, secretKey, name, description, created);
        subaccount.setId(id);

        return subaccount;
    }
}

