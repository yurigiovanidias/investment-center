import {Command} from "./command";

export const GetProfitCommand = "get_profit";

export interface GetProfit extends Command {
    symbol: string
}
