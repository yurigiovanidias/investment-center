import {Command} from "./command";

export const GetSnapshotsCommand = "get_snapshots";

export interface GetSnapshots extends Command {
    from_created: string,
    to_created: string,
    symbol: string,
}
