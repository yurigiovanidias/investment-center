import {Command} from "./command";

export const GetCommissionsCommand = "get_commissions";

export interface GetCommissions extends Command {}
