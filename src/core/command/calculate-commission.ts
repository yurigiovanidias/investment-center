import { Command } from "./command";

export const CalculateCommissionCommand = "calculate_commission";

export interface CalculateCommission extends Command {}
