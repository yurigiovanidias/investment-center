import {Command} from "./command";
import {Status} from "../value-object";

export const GetOrdersCommand = "get_orders";

export interface GetOrders extends Command {
    from_created: string,
    to_created: string,
    status: Array<Status>,
}
