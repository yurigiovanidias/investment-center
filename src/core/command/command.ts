import {AccountInfo} from "../value-object";

export interface Command {
    readonly command_name: string;
    aws_request_id: string
    account_info: AccountInfo;
    data: any;
}
