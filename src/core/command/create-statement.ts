import {Command} from "./command";
import {Type} from "../value-object";

export const CreateStatementCommand = "create_statement";

export interface CreateStatement extends Command {
    readonly input_value: number;
    readonly input_currency: string;
    readonly output_value: number;
    readonly output_currency: string;
    readonly statement_type: Type;
}
