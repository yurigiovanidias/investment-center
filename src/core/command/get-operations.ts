import {Command} from "./command";

export const GetOperationsCommand = "get_operations";

export interface GetOperations extends Command {
    from_created: string,
    to_created: string
}
