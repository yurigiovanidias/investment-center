import {Command} from "./command";

export const GetStatementsCommand = "get_statements";

export interface GetStatements extends Command {}
