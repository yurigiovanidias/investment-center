import {Command} from "./command";

export const GetAccountCommand = "get_account";

export interface GetAccount extends Command {}
