import {Command} from "./command";
import {Strategy} from "../entity/account";

export const GetStrategyAccountsCommand = "get_strategy_accounts";

export interface GetStrategyAccounts extends Command {
    readonly strategy: Strategy;
}
