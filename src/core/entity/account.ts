export type Accounts = Array<Account>;

export enum Strategy {
    PendingOrders = 1,
    Index ,
    CashCarry
}

export class Account {
    // @ts-ignore
    private id: number = 0;
    private strategy_id: Strategy = 1;
    private name: string = "";
    private api_key: string = "";
    private created: Date;

    constructor(created: Date = new Date()) {
        this.created = created;
    }

    getId(): number {
        return this.id;
    }

    setId(value: number) {
        this.id = value;
        return this;
    }

    getStrategyId(): number {
        return this.strategy_id;
    }

    setStrategyId(value: number) {
        this.strategy_id = value;
        return this;
    }

    getName(): string {
        return this.name;
    }

    setName(value: string): Account {
        this.name = value;
        return this;
    }

    getApiKey(): string {
        return this.api_key;
    }

    setApiKey(value: string): Account {
        this.api_key = value;
        return this;
    }

    getCreated(): Date {
        return this.created;
    }

    setCreated(value: Date): Account {
        this.created = value;
        return this;
    }
}