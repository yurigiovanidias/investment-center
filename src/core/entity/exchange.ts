export class Exchange {
    // @ts-ignore
    private id: number = 0;
    private name: string = "";
    private acronym: string = "";
    private maker_fee: number = 0;
    private taker_fee: number = 0;
    private active: boolean = false;
    private created: Date = new Date();
    private updated: Date | undefined;

    getId(): number {
        return this.id;
    }

    setId(value: number): Exchange {
        this.id = value;
        return this;
    }

    getName(): string {
        return this.name;
    }

    setName(value: string): Exchange {
        this.name = value;
        return this;
    }

    getAcronym(): string {
        return this.acronym;
    }

    setAcronym(value: string): Exchange {
        this.acronym = value;
        return this;
    }

    getMakerFee(): number {
        return this.maker_fee;
    }

    setMakerFee(value: number): Exchange {
        this.maker_fee = value;
        return this;
    }

    getTakerFee(): number {
        return this.taker_fee;
    }

    setTakerFee(value: number): Exchange {
        this.taker_fee = value;
        return this;
    }

    getActive(): boolean {
        return this.active;
    }

    setActive(value: boolean): Exchange {
        this.active = value;
        return this;
    }

    getCreated(): Date {
        return this.created;
    }

    setCreated(value: Date) {
        this.created = value;
    }
}
