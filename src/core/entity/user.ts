export class User {
    // @ts-ignore
    private id: number = 0;
    private first_name: string = "";
    private last_name: string = "";
    private email: string = "";
    private active: boolean = false;
    private created: Date = new Date();
    private updated: Date | undefined;

    getId(): number {
        return this.id;
    }

    setId(value: number): User {
        this.id = value;
        return this;
    }

    getFirstName(): string {
        return this.first_name;
    }

    setFirstName(value: string): User {
        this.first_name = value;
        return this;
    }

    getLastName(): string {
        return this.last_name;
    }

    setLastName(value: string): User {
        this.last_name = value;
        return this;
    }

    getEmail(): string {
        return this.email;
    }

    setEmail(value: string): User {
        this.email = value;
        return this;
    }

    getActive(): boolean {
        return this.active;
    }

    setActive(value: boolean): User {
        this.active = value;
        return this;
    }

    getCreated(): Date {
        return this.created;
    }

    setCreated(value: Date) {
        this.created = value;
    }
}
