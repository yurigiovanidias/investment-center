import {Subaccount} from "./subaccount";

export class ExchangeAccount {
    // @ts-ignore
    private id: number = 0;
    private user: number = 0;
    private exchange: number = 0;
    private maker_fee: number = 0;
    private taker_fee: number = 0;

    getId(): number {
        return this.id;
    }

    setId(value: number) {
        this.id = value;
        return this;
    }

    getUser(): number {
        return this.user;
    }

    setUser(value: number): ExchangeAccount {
        this.user = value;
        return this;
    }

    getExchange(): number {
        return this.exchange;
    }

    setExchange(value: number): ExchangeAccount {
        this.exchange = value;
        return this;
    }

    getMakerFee(): number {
        return this.maker_fee;
    }

    setMakerFee(value: number): ExchangeAccount {
        this.maker_fee = value;
        return this;
    }

    getTakerFee(): number {
        return this.taker_fee;
    }

    setTakerFee(value: number): ExchangeAccount {
        this.taker_fee = value;
        return this;
    }
}