export type Configurations = Array<Configuration>;
export type ConfigurationCollection = Map<string, string>;

export const ConfigurationsName = {
    Commission: "commission",
    MinAmountCommission: "min_amount_commission",
};

export class Configuration {
    // @ts-ignore
    private account_id: number = 0;
    private name: string = "";
    private value: string = "";
    private created: Date = new Date();
    private updated: Date = new Date();

    getAccountId(): number {
        return this.account_id;
    }

    setAccountId(value: number): Configuration {
        this.account_id = value;
        return this;
    }

    getName(): string {
        return this.name;
    }

    setName(value: string): Configuration {
        this.name = value;
        return this;
    }

    getValue(): string {
        return this.value;
    }

    setValue(value: string): Configuration {
        this.value = value;
        return this;
    }

    getCreated(): Date {
        return this.created;
    }

    setCreated(value: Date): Configuration {
        this.created = value;
        return this;
    }

    getUpdated(): Date {
        return this.created;
    }

    setUpdated(value: Date): Configuration {
        this.updated = value;
        return this;
    }
}
