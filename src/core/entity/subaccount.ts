export class Subaccount {
    // @ts-ignore
    private id: number = 0;
    private account_id: number = 0;
    private exchange_id: number = 0;
    private name: string = "";
    private description?: string = "";
    private api_key: string = "";
    private secret_key: string = "";
    private created: Date = new Date();

    getId(): number {
        return this.id;
    }

    setId(value: number): Subaccount {
        this.id = value;
        return this;
    }

    getAccountId(): number {
        return this.account_id;
    }

    setAccountId(value: number): Subaccount {
        this.account_id = value;
        return this;
    }

    getExchangeId(): number {
        return this.exchange_id;
    }

    setExchangeId(value: number): Subaccount {
        this.exchange_id = value;
        return this;
    }

    getName(): string {
        return this.name;
    }

    setName(value: string): Subaccount {
        this.name = value;
        return this;
    }

    getDescription(): string | undefined {
        return this.description;
    }

    setDescription(value: string | undefined): Subaccount {
        this.description = value;
        return this;
    }

    getApiKey(): string {
        return this.api_key;
    }

    setApiKey(value: string): Subaccount {
        this.api_key = value;
        return this;
    }

    getSecretKey(): string {
        return this.secret_key;
    }

    setSecretKey(value: string): Subaccount {
        this.secret_key = value;
        return this;
    }

    getCreated(): Date {
        return this.created;
    }

    setCreated(value: Date) {
        this.created = value;
    }
}
