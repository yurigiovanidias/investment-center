import {inject, injectable} from "inversify";
import {Core} from "../../config/types";
import {Account} from "../entity";
import {AccountInfo, Symbol} from "../value-object";
import {OperationFactory} from "../factory/operation-factory";
import {ExchangeService} from "../exchange/service";
import {SubaccountFactory} from "../factory/subaccount-factory";
import {InvalidApiKeyError} from "../error/error";
import {AccountRepository} from "../repository/account-repository";
import {CommissionRepository} from "../repository/commission-repository";
import {StatementRepository} from "../repository/statement-repository";

@injectable()
export class AccountService {
    private readonly accountRepository: AccountRepository;
    private readonly commissionRepository: CommissionRepository;
    private readonly statementRepository: StatementRepository;
    private readonly exchangeService: ExchangeService;
    private readonly operationFactory: OperationFactory;
    private readonly subaccountFactory: SubaccountFactory;

    constructor(
        @inject(Core.AccountRepository) accountRepository: AccountRepository,
        @inject(Core.CommissionRepository) commissionRepository: CommissionRepository,
        @inject(Core.StatementRepository) statementRepository: StatementRepository,
        @inject(Core.ExchangeService) exchangeService: ExchangeService,
        @inject(Core.OperationFactory) operationFactory: OperationFactory,
        @inject(Core.SubaccountFactory) subaccountFactory: SubaccountFactory,
    ) {
        this.accountRepository = accountRepository;
        this.commissionRepository = commissionRepository;
        this.statementRepository = statementRepository;
        this.exchangeService = exchangeService;
        this.operationFactory = operationFactory;
        this.subaccountFactory = subaccountFactory;
    }

    async getActiveAccountInfo(strategyId: number) {
        const accounts = await this.accountRepository.findActiveByStrategy(strategyId);
        const exchangeAccounts = [];

        for (const account of accounts) {
            const result = await this.exchangeService.getAccountAndSubaccount(account.getApiKey());

            exchangeAccounts.push(result);
        }

        const accountsInfo = [];

        for (const key in exchangeAccounts) {
            const account = accounts[key];

            const result = (new AccountInfo())
                .setAccount(account)
                .setExchangeAccount(exchangeAccounts[key])

            accountsInfo.push(result);
        }

        return accountsInfo;
    }

    /**
     *
     * @param apiKey
     */
    async getAccountInfo(apiKey: string) {
        const account = await this.accountRepository.findByApiKey(apiKey);

        if (account.getId() <= 0) {
            throw new InvalidApiKeyError();
        }

        const exchangeAccount = await this.exchangeService.getAccountAndSubaccount(apiKey);
        return (new AccountInfo())
            .setAccount(account)
            .setExchangeAccount(exchangeAccount)
    }

    async calculateProfit(account: Account, symbol: Symbol) {
        const [investment, balance] = await Promise.all([
            this.calculateInvestment(account.getId()),
            this.calculateBalance(account.getApiKey(), symbol)
        ]);

        return balance - investment;
    }

    private async calculateInvestment(accountId: number): Promise<number> {
        const statements = await this.statementRepository.findByAccountId(accountId);
        const commissions = await this.commissionRepository.findByAccountId(accountId);

        const investment = statements.reduce((acc, curr) => {
            if (curr.getInputCurrencyId() === "BRL") {
                return acc + curr.getIntegerInput();
            }

            return acc + curr.getIntegerOutput();
        }, 0);

        const commission = commissions.reduce((acc, curr) => {
            return acc + curr.getIntegerAmount();
        }, 0);

        return investment + commission;
    }

    private async calculateBalance(apiKey: string, symbol: Symbol) {
        const balances = await this.exchangeService.getBalance(apiKey);
        const indexes = await this.exchangeService.getIndex("", symbol);
        const index = indexes.find(index => index.getExchange() === "BRBT")!;

        const balanceBase = balances.find(balance => balance.getSymbol() === symbol.getBase());
        const balanceQuote = balances.find(balance => balance.getSymbol() === symbol.getQuote());

        console.log(balanceBase, balanceQuote);

        const totalBalance = balanceBase!.getDecimalTotal();
        const balanceInQuote = +(totalBalance * index.getPrice()).toFixed(8) * 1e8;

        return balanceInQuote + balanceQuote!.getIntegerTotal();
    }
}
