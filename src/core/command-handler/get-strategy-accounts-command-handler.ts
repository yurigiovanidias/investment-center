import {inject, injectable} from "inversify";
import {Core} from "../../config/types";
import {CommandHandler} from "./command-handler";
import {GetStrategyAccounts, GetStrategyAccountsCommand} from "../command";
import {EventBus} from "../event-bus/event-bus";
import {AccountService} from "../service/account-service";

@injectable()
export class GetStrategyAccountsCommandHandler implements CommandHandler {
    private eventBus: EventBus;
    private accountService: AccountService;

    constructor(
        @inject(Core.EventBus) eventBus: EventBus,
        @inject(Core.AccountService) accountService: AccountService,
    ) {
        this.eventBus = eventBus;
        this.accountService = accountService;
    }

    /**
     *
     * @param command
     */
    async handle(command: GetStrategyAccounts): Promise<void> {
        console.log(`Get strategy accounts command handler`);

        const strategy = command.strategy || 2;
        command.data = await this.accountService.getActiveAccountInfo(strategy);
    }

    getCommandName(): string {
        return GetStrategyAccountsCommand;
    }
}
