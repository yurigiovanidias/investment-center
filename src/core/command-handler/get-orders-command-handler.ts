import {inject, injectable} from "inversify";
import {Core} from "../../config/types";
import {CommandHandler} from "./command-handler";
import {GetOrders, GetOrdersCommand} from "../command";
import {EventBus} from "../event-bus/event-bus";
import {AccountService} from "../service/account-service";
import {ExchangeService} from "../exchange/service";
import {GetOrdersValidator} from "../validator";

@injectable()
export class GetOrdersCommandHandler implements CommandHandler {
    private eventBus: EventBus;
    private accountService: AccountService;
    private exchangeService: ExchangeService;
    private validator: GetOrdersValidator;

    constructor(
        @inject(Core.EventBus) eventBus: EventBus,
        @inject(Core.AccountService) accountService: AccountService,
        @inject(Core.ExchangeService) exchangeService: ExchangeService,
        @inject(Core.GetOrdersValidator) validator: GetOrdersValidator,
    ) {
        this.eventBus = eventBus;
        this.accountService = accountService;
        this.exchangeService = exchangeService;
        this.validator = validator;
    }

    /**
     *
     * @param command
     */
    async handle(command: GetOrders): Promise<void> {
        console.log(`Get orders command handler`);

        this.validator.validate(command);
        command.data = await this.exchangeService.getOrders(command.account_info.getAccount().getApiKey(), command.status, command.from_created, command.to_created);
    }

    getCommandName(): string {
        return GetOrdersCommand;
    }
}
