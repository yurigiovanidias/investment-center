import {inject, injectable} from "inversify";
import {Core} from "../../config/types";
import {CommandHandler} from "./command-handler";
import {CalculateCommission, CalculateCommissionCommand} from "../command";
import {EventBus} from "../event-bus/event-bus";
import {ConfigurationRepository} from "../repository/configuration-repository";
import {CommissionRepository} from "../repository/commission-repository";
import {ExchangeService} from "../exchange/service";
import {Type, Symbol, Subtype} from "../value-object";
import {CommissionFactory} from "../factory/commission-factory";
import {OperationFactory} from "../factory/operation-factory";
import {ConfigurationsName} from "../entity/configuration";
import {AccountService} from "../service/account-service";

@injectable()
export class CalculateCommissionCommandHandler implements CommandHandler {
    private accountService: AccountService;
    private commissionFactory: CommissionFactory;
    private commissionRepository: CommissionRepository;
    private configurationRepository: ConfigurationRepository;
    private eventBus: EventBus;
    private exchangeService: ExchangeService;
    private operationFactory: OperationFactory;

    constructor(
        @inject(Core.EventBus) eventBus: EventBus,
        @inject(Core.AccountService) accountService: AccountService,
        @inject(Core.ExchangeService) exchangeService: ExchangeService,
        @inject(Core.CommissionFactory) commissionFactory: CommissionFactory,
        @inject(Core.CommissionRepository) commissionRepository: CommissionRepository,
        @inject(Core.ConfigurationRepository) configurationRepository: ConfigurationRepository,
        @inject(Core.OperationFactory) operationFactory: OperationFactory,
    ) {
        this.accountService = accountService;
        this.commissionFactory = commissionFactory;
        this.commissionRepository = commissionRepository;
        this.configurationRepository = configurationRepository;
        this.operationFactory = operationFactory;
        this.eventBus = eventBus;
        this.exchangeService = exchangeService;
    }

    /**
     *
     * @param command
     */
    async handle(command: CalculateCommission): Promise<void> {
        // @ts-ignore
        const symbol = new Symbol("BTC_BRL");
        const profit = await this.accountService.calculateProfit(command.account_info.getAccount(), symbol);

        if (profit <= 0) {
            console.log(`Nonprofit account last month: ${profit}`);
            return;
        }

        const configurations = await this.configurationRepository.findByAccountId(command.account_info.getAccount().getId());
        const commissionConfiguration = +configurations.get(ConfigurationsName.Commission)! || 0;
        const minAmountConfiguration = +configurations.get(ConfigurationsName.MinAmountCommission)! || 0;
        const commissionAmount = commissionConfiguration * profit;

        if ((commissionAmount / 1e8) <= minAmountConfiguration) {
            console.log(`Minimum amount commission reached: ${minAmountConfiguration} - ${commissionAmount}`);
            return;
        }

        const commission = this.commissionFactory.createWithoutId(
            command.account_info.getAccount().getId(),
            symbol.getQuote(),
            commissionAmount,
        );

        const result = await this.commissionRepository.save(commission);
        const operation = this.operationFactory.createWithoutId(
            command.account_info.getExchangeAccount().getId(),
            undefined,
            commission.getCurrencyId(),
            commissionAmount,
            Type.Debit,
            Subtype.Quote,
            `Commission #${commission.getId()}`,
            new Date()
        );

        await this.exchangeService.createOperation(command.account_info.getAccount().getApiKey(), operation);

        console.log(commission);

        command.data = commission;
    }

    getCommandName(): string {
        return CalculateCommissionCommand;
    }
}
