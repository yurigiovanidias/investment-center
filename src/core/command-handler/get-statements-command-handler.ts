import {inject, injectable} from "inversify";
import {Core} from "../../config/types";
import {CommandHandler} from "./command-handler";
import {GetStatements, GetStatementsCommand} from "../command";
import {EventBus} from "../event-bus/event-bus";
import {ExchangeService} from "../exchange/service";
import {OperationFactory} from "../factory/operation-factory";
import {StatementRepository} from "../repository/statement-repository";
import {StatementFactory} from "../factory/statement-factory";

@injectable()
export class GetStatementsCommandHandler implements CommandHandler {
    private eventBus: EventBus;
    private exchangeService: ExchangeService;
    private statementFactory: StatementFactory;
    private statementRepository: StatementRepository;

    constructor(
        @inject(Core.EventBus) eventBus: EventBus,
        @inject(Core.ExchangeService) exchangeService: ExchangeService,
        @inject(Core.StatementRepository) statementRepository: StatementRepository,
        @inject(Core.StatementFactory) statementFactory: StatementFactory,
    ) {
        this.eventBus = eventBus;
        this.exchangeService = exchangeService;
        this.statementRepository = statementRepository;
        this.statementFactory = statementFactory;
    }

    /**
     *
     * @param command
     */
    async handle(command: GetStatements): Promise<void> {
        console.log(`Get statements command handler`);

        command.data = await this.statementRepository.findByAccountId(command.account_info.getAccount().getId());
    }

    getCommandName(): string {
        return GetStatementsCommand;
    }
}
