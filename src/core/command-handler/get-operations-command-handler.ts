import {inject, injectable} from "inversify";
import {Core} from "../../config/types";
import {CommandHandler} from "./command-handler";
import {GetOperations, GetOperationsCommand} from "../command";
import {EventBus} from "../event-bus/event-bus";
import {AccountService} from "../service/account-service";
import {ExchangeService} from "../exchange/service";
import {GetOperationsValidator} from "../validator";

@injectable()
export class GetOperationsCommandHandler implements CommandHandler {
    private eventBus: EventBus;
    private accountService: AccountService;
    private exchangeService: ExchangeService;
    private validator: GetOperationsValidator;

    constructor(
        @inject(Core.EventBus) eventBus: EventBus,
        @inject(Core.AccountService) accountService: AccountService,
        @inject(Core.ExchangeService) exchangeService: ExchangeService,
        @inject(Core.GetOperationsValidator) validator: GetOperationsValidator,
    ) {
        this.eventBus = eventBus;
        this.accountService = accountService;
        this.exchangeService = exchangeService;
        this.validator = validator;
    }

    /**
     *
     * @param command
     */
    async handle(command: GetOperations): Promise<void> {
        console.log(`Get operations command handler`);

        this.validator.validate(command);
        command.data = await this.exchangeService.getOperations(command.account_info.getAccount().getApiKey(), command.from_created, command.to_created);
    }

    getCommandName(): string {
        return GetOperationsCommand;
    }
}
