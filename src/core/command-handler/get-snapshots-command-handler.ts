import {inject, injectable} from "inversify";
import {Core} from "../../config/types";
import {CommandHandler} from "./command-handler";
import {GetSnapshots, GetSnapshotsCommand} from "../command";
import {EventBus} from "../event-bus/event-bus";
import {AccountService} from "../service/account-service";
import {ExchangeService} from "../exchange/service";
import {Symbol} from "../value-object";
import {Exchanges} from "../../data-provider/exchange/constants";
import {GetSnapshotsValidator} from "../validator";

@injectable()
export class GetSnapshotsCommandHandler implements CommandHandler {
    private eventBus: EventBus;
    private accountService: AccountService;
    private exchangeService: ExchangeService;
    private validator: GetSnapshotsValidator;

    constructor(
        @inject(Core.EventBus) eventBus: EventBus,
        @inject(Core.AccountService) accountService: AccountService,
        @inject(Core.ExchangeService) exchangeService: ExchangeService,
        @inject(Core.GetSnapshotsValidator) validator: GetSnapshotsValidator,
    ) {
        this.eventBus = eventBus;
        this.accountService = accountService;
        this.exchangeService = exchangeService;
        this.validator = validator;
    }

    /**
     *
     * @param command
     */
    async handle(command: GetSnapshots): Promise<void> {
        console.log(`Get snapshots command handler`);

        this.validator.validate(command);

        /**
         * @todo Fazer com que o symbol seja informado pelo client
         */
        const commandSymbol = command.symbol || "BTC_BRL";
        const symbol = new Symbol(commandSymbol);
        const [snapshots, candles] = await Promise.all([
            this.exchangeService.getSnapshots(command.account_info.getAccount().getApiKey(), command.from_created, command.to_created),
            this.exchangeService.getCandles(Exchanges.binance.acronym, symbol, command.from_created, command.to_created),
        ]);
            command.data = {
            snapshots: snapshots,
            candles: candles,
        };
    }

    private getAcronymByExchangeId(exchangeId: number): string {
        const exchange = Object.values(Exchanges).find(row => {
            // @ts-ignore
            return row.id === exchangeId;
        });

        // @ts-ignore
        return exchange.acronym;
    }

    getCommandName(): string {
        return GetSnapshotsCommand;
    }
}
