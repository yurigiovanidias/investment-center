import {inject, injectable} from "inversify";
import {Core} from "../../config/types";
import {CommandHandler} from "./command-handler";
import {GetProfit, GetProfitCommand} from "../command";
import {EventBus} from "../event-bus/event-bus";
import {ExchangeService} from "../exchange/service";
import {StatementRepository} from "../repository/statement-repository";
import {StatementFactory} from "../factory/statement-factory";
import {AccountService} from "../service/account-service";
import {Symbol} from "../value-object";

@injectable()
export class GetProfitCommandHandler implements CommandHandler {
    private eventBus: EventBus;
    private accountService: AccountService;

    constructor(
        @inject(Core.EventBus) eventBus: EventBus,
        @inject(Core.AccountService) accountService: AccountService,
    ) {
        this.eventBus = eventBus;
        this.accountService = accountService;
    }

    /**
     *
     * @param command
     */
    async handle(command: GetProfit): Promise<void> {
        console.log(`Get statements command handler`);

        const symbol = new Symbol(command.symbol)
        command.data = await this.accountService.calculateProfit(command.account_info.getAccount(), symbol);
    }

    getCommandName(): string {
        return GetProfitCommand;
    }
}
