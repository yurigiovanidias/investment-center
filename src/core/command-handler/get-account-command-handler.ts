import {inject, injectable} from "inversify";
import {Core} from "../../config/types";
import {CommandHandler} from "./command-handler";
import {GetAccount, GetAccountCommand} from "../command";
import {EventBus} from "../event-bus/event-bus";
import {AccountService} from "../service/account-service";

@injectable()
export class GetAccountCommandHandler implements CommandHandler {
    private eventBus: EventBus;
    private accountService: AccountService;

    constructor(
        @inject(Core.EventBus) eventBus: EventBus,
        @inject(Core.AccountService) accountService: AccountService,
    ) {
        this.eventBus = eventBus;
        this.accountService = accountService;
    }

    /**
     *
     * @param command
     */
    async handle(command: GetAccount): Promise<void> {
        console.log(`Get account command handler`);

        command.data = command.account_info;
    }

    getCommandName(): string {
        return GetAccountCommand;
    }
}
