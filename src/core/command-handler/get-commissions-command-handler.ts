import {inject, injectable} from "inversify";
import {Core} from "../../config/types";
import {CommandHandler} from "./command-handler";
import {GetCommissions, GetCommissionsCommand} from "../command";
import {EventBus} from "../event-bus/event-bus";
import {ExchangeService} from "../exchange/service";
import {CommissionRepository} from "../repository/commission-repository";

@injectable()
export class GetCommissionsCommandHandler implements CommandHandler {
    private eventBus: EventBus;
    private exchangeService: ExchangeService;
    private commissionRepository: CommissionRepository;

    constructor(
        @inject(Core.EventBus) eventBus: EventBus,
        @inject(Core.ExchangeService) exchangeService: ExchangeService,
        @inject(Core.CommissionRepository) commissionRepository: CommissionRepository,
    ) {
        this.eventBus = eventBus;
        this.exchangeService = exchangeService;
        this.commissionRepository = commissionRepository;
    }

    /**
     *
     * @param command
     */
    async handle(command: GetCommissions): Promise<void> {
        console.log(`Get statements command handler`);

        command.data = await this.commissionRepository.findByAccountId(command.account_info.getAccount().getId());
    }

    getCommandName(): string {
        return GetCommissionsCommand;
    }
}
