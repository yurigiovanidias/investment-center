import {Command} from "../command/command";

export type CommandHandlerCollection = Map<string, CommandHandler>;

export interface CommandHandler {
    /**
     *
     * @param command
     */
    handle(command: Command): void;

    getCommandName(): string;
}
