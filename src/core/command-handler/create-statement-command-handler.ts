import {inject, injectable} from "inversify";
import {Core} from "../../config/types";
import {CommandHandler} from "./command-handler";
import {CreateStatement, CreateStatementCommand} from "../command";
import {CreatedStatement} from "../event/created-statement";
import {EventBus} from "../event-bus/event-bus";
import {ExchangeService} from "../exchange/service";
import {StatementType, Subtype, Type} from "../value-object";
import {OperationFactory} from "../factory/operation-factory";
import {StatementRepository} from "../repository/statement-repository";
import {StatementFactory} from "../factory/statement-factory";
import {CreateStatementValidator} from "../validator";

@injectable()
export class CreateStatementCommandHandler implements CommandHandler {
    private eventBus: EventBus;
    private exchangeService: ExchangeService;
    private operationFactory: OperationFactory;
    private statementFactory: StatementFactory;
    private statementRepository: StatementRepository;
    private validator: CreateStatementValidator;

    constructor(
        @inject(Core.EventBus) eventBus: EventBus,
        @inject(Core.ExchangeService) exchangeService: ExchangeService,
        @inject(Core.StatementRepository) statementRepository: StatementRepository,
        @inject(Core.OperationFactory) operationFactory: OperationFactory,
        @inject(Core.StatementFactory) statementFactory: StatementFactory,
        @inject(Core.CreateStatementValidator) validator: CreateStatementValidator,
    ) {
        this.eventBus = eventBus;
        this.exchangeService = exchangeService;
        this.statementRepository = statementRepository;
        this.operationFactory = operationFactory;
        this.statementFactory = statementFactory;
        this.validator = validator;
    }

    /**
     *
     * @param command
     */
    async handle(command: CreateStatement): Promise<void> {
        console.log(`Create statement command handler`);

        this.validator.validate(command);

        const inputValue = command.input_value * 1e8;
        const outputValue = command.output_value * 1e8;

        const statement = this.statementFactory.createWithoutId(
            command.account_info.getAccount().getId(),
            command.input_currency,
            inputValue,
            command.output_currency,
            outputValue,
            <StatementType><any>command.statement_type,
            new Date(),
        );
        const resultStatement = await this.statementRepository.save(statement);

        if (typeof resultStatement === "undefined" || resultStatement.length <= 0) {
            throw new Error(`Error while saving statement: ${JSON.stringify(statement)}`);
        }

        statement.setId(resultStatement[0]);

        let subtype = Subtype.Base;
        let type = Type.Credit;

        if (command.output_currency === "BRL") {
            subtype = Subtype.Quote;
        }

        if (<StatementType><string>command.statement_type === StatementType.Withdraw) {
            type = Type.Debit;
        }

        const operation = this.operationFactory.createWithoutId(
            command.account_info.getExchangeAccount().getId(),
            undefined,
            command.output_currency,
            outputValue,
            type,
            subtype,
            `Statement #${statement.getId()}`,
            new Date()
        );
        const result = await this.exchangeService.createOperation(command.account_info.getAccount().getApiKey(), operation);
        const event = new CreatedStatement(statement);

        await this.eventBus.dispatch(event.getEventName(), event);

        command.data = {
            statement: statement,
            operation: operation,
        };
    }

    getCommandName(): string {
        return CreateStatementCommand;
    }
}
