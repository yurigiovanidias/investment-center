import {injectable} from "inversify";
import {CommandValidator} from "./validator";
import {GetOrders} from "../command";
import {Status} from "../value-object";

@injectable()
export class GetOrdersValidator implements CommandValidator {
    validate(command: GetOrders): void {
        const from = new Date(command.from_created);
        const to = new Date(command.to_created);

        if (typeof command.from_created === "undefined" || isNaN(from.getTime())) {
            throw new Error("From created can not be empty");
        }

        if (typeof command.to_created === "undefined" || isNaN(to.getTime())) {
            throw new Error("To created can not be empty");
        }

        if (from.getTime() >= to.getTime()) {
            throw new Error("From date must be less than To date");
        }

        command.status.map(status => {
            if (!Object.values(Status).includes(status)) {
                throw new Error("Invalid status");
            }
        });

        return;
    }
}
