import {injectable} from "inversify";
import {CommandValidator} from "./validator";
import {GetSnapshots} from "../command";

@injectable()
export class GetSnapshotsValidator implements CommandValidator {
    validate(command: GetSnapshots): void {
        const from = new Date(command.from_created);
        const to = new Date(command.to_created);

        if (typeof command.from_created === "undefined" || isNaN(from.getTime())) {
            throw new Error("From created can not be empty");
        }

        if (typeof command.to_created === "undefined" || isNaN(to.getTime())) {
            throw new Error("To created can not be empty");
        }

        if (from.getTime() >= to.getTime()) {
            throw new Error("From date must be less than To date");
        }

        return;
    }
}
