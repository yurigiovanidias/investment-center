import {injectable} from "inversify";
import {CommandValidator} from "./validator";
import {CreateStatement} from "../command";
import {Type} from "../value-object";

@injectable()
export class CreateStatementValidator implements CommandValidator {
    validate(command: CreateStatement): void {
        if (typeof command.input_value === "undefined" || typeof command.input_value !== "number") {
            throw new Error("Input value must be a number");
        }

        if (typeof command.input_currency === "undefined" || typeof command.input_currency !== "string") {
            throw new Error("Input currency must be a string");
        }

        if (typeof command.output_value === "undefined" || typeof command.output_value !== "number") {
            throw new Error("Output value must be a number");
        }

        if (typeof command.output_currency === "undefined" || typeof command.output_currency !== "string") {
            throw new Error("Output currency must be a string");
        }

        if (typeof command.statement_type === "undefined" || !Object.values(Type).includes(command.statement_type)) {
            throw new Error("Statement type must be a Type: credit or debit");
        }

        return;
    }
}
