import {Command} from "../command/command";

export interface CommandValidator {
    validate(command: Command): void;
}
