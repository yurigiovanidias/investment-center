import {Container} from 'inversify';
import "reflect-metadata";
import ioc from '../config/ioc';
import {Config, loadConfig} from "../config/config";

let booted = false;
let container: any = undefined;

export class Application {
    protected booted: boolean = false;
    protected container: Container;
    protected config: Config;

    public constructor() {
        this.booted = booted;
        this.config = loadConfig();
        this.container = container;
    }

    public async run(): Promise<void> {
        await this.boot();
        return this._run();
    }

    public async boot(): Promise<void> {
        if (this.booted) {
            console.log("Application already booted");
            return;
        }

        this.container = new Container({
            'defaultScope': 'Singleton'
        });

        // build the container
        await ioc(this.container, this.config);

        this._boot();

        this.booted = true;
        booted = true;
        container = this.container;
    }

    public get<T>(service: string|symbol): T {
        return this.container.get<T>(service);
    }

    protected _boot(): void {};

    protected _run(): void {};
}
