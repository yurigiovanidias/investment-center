export interface StorageConfig {
    database: string,
    host: string,
    password: string,
    port: number,
    user: string,
    multipleStatements: boolean,
}
