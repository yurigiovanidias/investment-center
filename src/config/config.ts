const dotenv = require('dotenv');
import {StorageConfig} from "./storage/config";

export interface Config {
    env: string,
    database: StorageConfig,
}

export const loadConfig = (): Config => {
    if (process.env.NODE_ENV !== 'production') {
        dotenv.config();
    }

    return {
        env: <string>process.env.NODE_ENV,
        database: {
            database: <string>process.env.STORAGE_DATABASE,
            host: <string>process.env.STORAGE_HOST,
            user: <string>process.env.STORAGE_USER,
            password: <string>process.env.STORAGE_PASSWORD,
            port: <number><any>process.env.STORAGE_PORT,
            multipleStatements: true,
        }
    }
}
