echo "Installing dependencies"
npm install --loglevel verbose
#npm prune --production
echo "Removing old files"
rm -rf ../../../dist
echo "Compiling TS files"
npx tsc
echo "Deploying compiled files"
sls deploy -c src/config/deploy/serverless.yml -s production
