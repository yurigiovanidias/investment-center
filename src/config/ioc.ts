import {Container} from "inversify";
import ioc from "../data-provider/ioc";
import {Config} from "./config";
import {Core} from "./types";
import {
    CreateStatementCommandHandler,
    CalculateCommissionCommandHandler,
    GetAccountCommandHandler,
    GetStrategyAccountsCommandHandler,
    GetOperationsCommandHandler,
    GetSnapshotsCommandHandler,
    GetOrdersCommandHandler,
    GetStatementsCommandHandler,
    GetCommissionsCommandHandler,
    GetProfitCommandHandler,
} from "../core/command-handler";
import {AccountService} from "../core/service/account-service";
import {StatementListener} from "../core/event-listener/statement-listener";
import {CommissionFactory} from "../core/factory/commission-factory";
import {OperationFactory} from "../core/factory/operation-factory";
import {SubaccountFactory} from "../core/factory/subaccount-factory";
import {StatementFactory} from "../core/factory/statement-factory";
import {ConfigurationFactory} from "../core/factory/configuration-factory";
import {
    CreateStatementValidator,
    GetOperationsValidator,
    GetOrdersValidator,
    GetSnapshotsValidator
} from "../core/validator";

export default async function(container: Container, config: Config): Promise<void> {
    registerCore(container);
    await registerDataProvider(container, config);
}

const registerCore = (container: Container) => {
    container.bind<AccountService>(Core.AccountService).to(AccountService);
    container.bind<ConfigurationFactory>(Core.ConfigurationFactory).to(ConfigurationFactory);
    container.bind<CommissionFactory>(Core.CommissionFactory).to(CommissionFactory);
    container.bind<OperationFactory>(Core.OperationFactory).to(OperationFactory);
    container.bind<StatementFactory>(Core.StatementFactory).to(StatementFactory);
    container.bind<SubaccountFactory>(Core.SubaccountFactory).to(SubaccountFactory);
    container.bind<CreateStatementCommandHandler>(Core.CommandHandler.Public).to(CreateStatementCommandHandler);
    container.bind<CalculateCommissionCommandHandler>(Core.CommandHandler.Public).to(CalculateCommissionCommandHandler);
    container.bind<GetAccountCommandHandler>(Core.CommandHandler.Public).to(GetAccountCommandHandler);
    container.bind<GetCommissionsCommandHandler>(Core.CommandHandler.Public).to(GetCommissionsCommandHandler);
    container.bind<GetStrategyAccountsCommandHandler>(Core.CommandHandler.Private).to(GetStrategyAccountsCommandHandler);
    container.bind<GetOperationsCommandHandler>(Core.CommandHandler.Public).to(GetOperationsCommandHandler);
    container.bind<GetOrdersCommandHandler>(Core.CommandHandler.Public).to(GetOrdersCommandHandler);
    container.bind<GetProfitCommandHandler>(Core.CommandHandler.Public).to(GetProfitCommandHandler);
    container.bind<GetSnapshotsCommandHandler>(Core.CommandHandler.Public).to(GetSnapshotsCommandHandler);
    container.bind<GetStatementsCommandHandler>(Core.CommandHandler.Public).to(GetStatementsCommandHandler);
    container.bind<StatementListener>(Core.EventSubscriber).to(StatementListener);
    container.bind<CreateStatementValidator>(Core.CreateStatementValidator).to(CreateStatementValidator);
    container.bind<GetOperationsValidator>(Core.GetOperationsValidator).to(GetOperationsValidator);
    container.bind<GetOrdersValidator>(Core.GetOrdersValidator).to(GetOrdersValidator);
    container.bind<GetSnapshotsValidator>(Core.GetSnapshotsValidator).to(GetSnapshotsValidator);
};

const registerDataProvider = async (container: Container, config: Config) => {
    return ioc(container, config);
};
