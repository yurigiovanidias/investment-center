import {handle} from "../../../src/entrypoint/http/api";
import {APIGatewayProxyEvent, Context} from "aws-lambda";
import {
    CreateStatement,
    CreateStatementCommand,
    GetAccountCommand,
    GetOperationsCommand, GetOrders, GetOrdersCommand, GetSnapshotsCommand,
    GetStrategyAccounts,
    GetStrategyAccountsCommand,
} from "../../../src/core/command";

const getEventByAction = (action: string): APIGatewayProxyEvent | undefined => {
    let event: APIGatewayProxyEvent | undefined;

    if (action === 'http:create-statement') {
        event = <APIGatewayProxyEvent> {
            body: JSON.stringify(<CreateStatement>{
                command_name: CreateStatementCommand,
                input_value: 0.1,
                input_currency: "BTC",
                output_value: 25000,
                output_currency: "BRL",
            }),
        }
    }

    if (action === 'http:get-account') {
        event = <APIGatewayProxyEvent> {
            body: JSON.stringify(<GetStrategyAccounts>{
                command_name: GetAccountCommand,
            }),
        }
    }

    if (action === 'http:get-commissions') {
        event = <APIGatewayProxyEvent> {
            body: JSON.stringify({
                command_name:"get_commissions"
            }),
        }
    }

    if (action === 'http:get-operations') {
        event = <APIGatewayProxyEvent> {
            body: JSON.stringify({
                command_name: GetOperationsCommand,
                from_created: "2021-09-20",
                to_created: "2021-09-19",
            }),
        }
    }

    if (action === 'http:get-orders') {
        event = <APIGatewayProxyEvent> {
            body: JSON.stringify(<GetOrders>{
                command_name: GetOrdersCommand,
                from_created: "2021-07-13",
                to_created: "2021-07-15",
                status: ["executed"],
            }),
        }
    }

    if (action === 'http:get-profit') {
        event = <APIGatewayProxyEvent> {
            body: JSON.stringify({
                "command_name":"get_profit","symbol":"BTC_BRL"
            }),
        }
    }

    if (action === 'http:get-snapshots') {
        event = <APIGatewayProxyEvent> {
            body: JSON.stringify({
                "command_name":"get_snapshots","from_created":"2021-08-01","to_created":"2021-08-31"
            }),
        }
    }

    if (action === 'http:get-statements') {
        event = <APIGatewayProxyEvent> {
            body: JSON.stringify({
                "command_name":"get_statements"
            }),
        }
    }

    if (typeof event !== "undefined") {
        event.headers = {
            // "x-api-key": "66c521d2-da4c-4fdf-bcb0-60f5963af6d6",
            // "x-api-key": "599f34e3-a248-4db4-9d96-e43a701cb121",
            "x-api-key": "7ea6ca82-6071-435a-855a-c5f830cca6f2",
        };
        //@ts-ignore
        event.requestContext = {
            requestId: `${(new Date).getTime()}`,
        };
    }

    return event;
};

(async () => {
    const ACTION = process.env.npm_lifecycle_event!;
    const event = getEventByAction(ACTION);
    const context = <Context>{};

    if(typeof event === "undefined") {
        console.log(`Event not found: ${ACTION}`);
        return;
    }

    try {
        const res = await handle(event, context);
        console.log(res);
    }
    catch (e) {
        console.log(e);
    }
})();
