import {handle} from "../../../src/entrypoint/http/papi";
import {APIGatewayProxyEvent, Context} from "aws-lambda";
import {
    GetStrategyAccounts,
    GetStrategyAccountsCommand
} from "../../../src/core/command";

const getEventByAction = (action: string): APIGatewayProxyEvent | undefined => {
    let event: APIGatewayProxyEvent | undefined;

    if (action === 'http:get-strategy-accounts') {
        event = <APIGatewayProxyEvent> {
            body: JSON.stringify(<GetStrategyAccounts>{
                command_name: GetStrategyAccountsCommand,
                // strategy: 1,
            }),
        }
    }

    if (typeof event !== "undefined") {
        event.headers = {
            "x-api-key": "R7VplpOCvr6EOMyz6jrRA13biamb4QQH8SkdvZxb",
        };
        //@ts-ignore
        event.requestContext = {
            requestId: `${(new Date).getTime()}`,
        };
    }

    return event;
};

(async () => {
    const ACTION = process.env.npm_lifecycle_event!;
    const event = getEventByAction(ACTION);
    const context = <Context>{};

    if(typeof event === "undefined") {
        console.log(`Event not found: ${ACTION}`);
        return;
    }

    try {
        const res = await handle(event, context);
        console.log(res);
    }
    catch (e) {
        console.log(e);
    }
})();
