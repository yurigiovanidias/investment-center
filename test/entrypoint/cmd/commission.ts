import {calculate} from "../../../src/entrypoint/cmd/commission";
import {ScheduledEvent} from "aws-lambda";

const getEventByAction = (action: string): ScheduledEvent | undefined => {
    let event: ScheduledEvent | undefined;

    if (action === 'cmd:calculate-commission') {
        event = <ScheduledEvent>{};
    }

    return event;
};

(async () => {
    const ACTION = process.env.npm_lifecycle_event!;
    const event = getEventByAction(ACTION);

    if(typeof event === "undefined") {
        return;
    }

    try {
        console.log(new Date());
        const res = await calculate(event);
    }
    catch (e) {
        console.log(e);
    }
})();
